/// <summary>
/// Enum MAAColoresC01 (ID 50102).
/// </summary>
enum 55102 "MAAColoresC01"
{
    value(0; Blanco)
    {
        Caption = 'Blanco';
    }
    value(1; Rojo)
    {
        Caption = 'Rojo';
    }
    value(2; Gris)
    {
        Caption = 'Gris';
    }
    value(3; Negro)
    {
        Caption = 'Negro';
    }
}
