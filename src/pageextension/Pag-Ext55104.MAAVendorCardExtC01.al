/// <summary>
/// PageExtension MAAVendorCardC01 (ID 55104) extends Record Vendor Card.
/// </summary>
pageextension 55104 "MAAVendorCardExtC01" extends "Vendor Card"
{
    actions
    {
        addlast(processing)
        {
            //EJERCICIO 1. Al pulsar el botón que me clone la ficha de proveedor a cliente
            action(MAAClonarAClienteC01)
            {
                ApplicationArea = All;
                Caption = 'Clonar a cliente';
                Image = Customer;

                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                begin
                    if rlCustomer.Get(Rec."No.") then begin
                        if Confirm('El cliente %1 ya existe. ¿Desea sobreescribirlo?', false, rlCustomer."No.") then begin
                            //Como ya existía lo modificamos
                            rlCustomer.TransferFields(Rec, true, true);
                            rlCustomer.Modify(true);
                            Message('Cliente modificado correctamente.');
                        end else begin
                            Message('Proceso cancelado por el usuario.');
                        end;
                    end else begin
                        //Como no existía todavía hacemos Init-Insert
                        rlCustomer.Init();
                        rlCustomer.TransferFields(Rec, true, true);
                        rlCustomer.Insert(true);
                        Message('Cliente creado correctamente.');
                    end;
                end;
            }
        }
    }
}
