/// <summary>
/// Codeunit "MAAFuncionesC01" (ID 50100). Se ejecutará cuando instalemos la App.
/// </summary>
codeunit 55100 "MAAInstallAppC01"
{
    Subtype = Install; // Se ejecutará automáticamente cuando instalemos la App

    trigger OnInstallAppPerCompany() // Se ejecuta tantas veces como empresas tenga
    begin

    end;

    trigger OnInstallAppPerDatabase() // Se ejecuta una vez por cada BBDD. Para tablas comunes a todas las empresas.
    begin

    end;
}
