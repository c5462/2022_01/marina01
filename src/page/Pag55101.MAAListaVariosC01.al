/// <summary>
/// Page "MAAListaVariosC01" (ID 50101).
/// </summary>
page 55101 MAAListaVariosC01
{
    ApplicationArea = All;
    Caption = 'Lista Vacunas y Otros MAA';
    AboutTitle = 'Lista Tabla Varios';
    AboutText = 'Se usará para mostrar una lista donde tenemos varios tipos de registros de Vacunas Covid';
    AdditionalSearchTerms = 'Curso 01';
    PageType = List;
    SourceTable = MAAVariosC01;
    UsageCategory = Lists;
    //Le indicamos lo siguiente, ya que las modificaciones se harán en la ficha (hay casos en los que pondremos que la Lista pueda ser editable,
    //y podremos hacer modificaciones en la propia Lista)
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;
    Editable = false;
    //Relacionamos la lista con la ficha
    CardPageId = MAACardVariosC01;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Indica si el tipo de registro es Vacuna u Otros.';
                    ApplicationArea = All;
                }
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Indica el Código del registro.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Descripción del registro Vacuna u Otros.';
                    ApplicationArea = All;
                }
                field(PeriodoSegundaDosis; Rec.PeriodoSegundaDosis)
                {
                    ToolTip = 'Indica el tiempo entre la primera y la segunda dosis de una vacuna.';
                    ApplicationArea = All;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Indica si el registro está o no bloqueado.';
                    ApplicationArea = All;
                }
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
            }
        }
    }
}
