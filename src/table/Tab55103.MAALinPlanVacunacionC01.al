/// <summary>
/// Table "MAALinPlanVacunacionC01" (ID 50103). Líneas del documento del Plan de Vacunación.
/// </summary>
table 55103 MAALinPlanVacunacionC01
{
    Caption = 'Lin. Plan de Vacunación';
    DataClassification = SystemMetadata;
    //Para que se abra la página MAASubPageLinVacunacionC01 cuando pulsamos en un campo calculado de esta tabla
    DrillDownPageId = MAASubPageLinVacunacionC01;

    fields
    {
        field(1; CodigoLineas; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; NoLinea; Integer)
        {
            Caption = 'Nº Linea';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; ClienteVacuna; Code[20])
        {
            Caption = 'Cliente a Vacunar';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = Customer."No.";

            //Para que ponga la misma fecha en líneas que en cabeceras, al introducir el cod. cliente
            trigger OnValidate()
            var
                rlMAACabPlanVacunacionC01: Record MAACabPlanVacunacionC01;
            begin
                if Rec.FechaVacuna = 0D then begin // Si todavía no hay fecha introducida que me la traiga
                    if rlMAACabPlanVacunacionC01.Get(Rec.CodigoLineas) then begin
                        Rec.Validate(FechaVacuna, rlMAACabPlanVacunacionC01.FechaInicioVacunacionPlan);
                    end;
                end;
            end;
        }
        field(4; FechaVacuna; Date)
        {
            Caption = 'Fecha de Vacunación';
            DataClassification = OrganizationIdentifiableInformation;

            //Al introducir fecha de vacuna que calcule la fecha de la segunda dosis
            trigger OnValidate()
            begin
                CalcularFechaSegundaVacunaF();
            end;
        }
        field(5; CodigoVacuna; Code[20])
        {
            Caption = 'Codigo Vacuna';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = MAAVariosC01.Codigo where(Tipo = const(Vacuna), Bloqueado = const(false));

            //Al introducir el codigo de vacuna que calcule la fecha de la segunda dosis
            trigger OnValidate()
            begin
                CalcularFechaSegundaVacunaF();
            end;
        }
        field(6; CodigoOtros; Code[20])
        {
            Caption = 'Codigo Otros';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = MAAVariosC01.Codigo where(Tipo = const(Otros), Bloqueado = const(false));
        }
        field(7; FechaSegundaVacuna; Date)
        {
            Caption = 'Fecha Segunda Vacunación';
            DataClassification = OrganizationIdentifiableInformation;
        }
    }
    keys
    {
        // Clave principal con 2 segmentos:
        key(PK; CodigoLineas, NoLinea) //CodigoLineas para poder filtrar con mi cabecera
                                       //NoLinea para poder rellenar mas de una linea en mi documento
        {
            Clustered = true;
        }
    }

    //----------------------- Zona de procedures -----------------------//
    /// <summary>
    /// Devuelve el nombre del cliente a vacunar. 
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreClienteF(): Text
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(Rec.ClienteVacuna) then begin
            exit(rlCustomer.Name);
        end;
    end;


    /// <summary>
    /// Devuelve la descripción de la Vacuna o de Otros, en función de los parámetros Tipo y Código.
    /// </summary>
    /// <param name="pTipo">Enum MAATipoC01.</param>
    /// <param name="pCodVarios">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure DescripcionVariosF(pTipo: Enum MAATipoC01; pCodVarios: Code[20]): Text
    var
        rlMAAVariosC01: Record MAAVariosC01;
    begin
        if rlMAAVariosC01.Get(pTipo, pCodVarios) then begin
            exit(rlMAAVariosC01.Descripcion);
        end;
    end;


    /// <summary>
    /// CalcularFechaSegundaVacunaF. Calcula la fecha de la segunda dosis de la vacuna.
    /// </summary>
    procedure CalcularFechaSegundaVacunaF()
    var
        rlMAAVariosC01: Record MAAVariosC01;
        IsHandled: Boolean;
    begin
        //Creamos evento OnBefore para que los clientes se puedan suscribir a él
        OnBeforeCalcularFechaSegundaVacunaF(Rec, xRec, rlMAAVariosC01, IsHandled);

        if not IsHandled then begin // Se ejecuta si NO está manejado por otro usuario (si no hay nadie suscrito)
            if Rec.FechaVacuna <> 0D then begin // Controlamos que FechaVacuna no sea 0, porque 'CalcDate' no se puede hacer sobre una fecha vacía
                if rlMAAVariosC01.Get(rlMAAVariosC01.Tipo::Vacuna, Rec.CodigoVacuna) then begin
                    Rec.Validate(FechaSegundaVacuna, CalcDate(rlMAAVariosC01.PeriodoSegundaDosis, Rec.FechaVacuna));
                end;
            end;
            //Creamos evento OnAfter para que los clientes se puedan suscribir a él
            OnAfterCalcularFechaSegundaVacunaF(Rec, xRec, rlMAAVariosC01);
        end;
    end;

    //----------------------- Eventos publicadores (suscripciones propias) -----------------------//

    [IntegrationEvent(false, false)] // Siempre es (false, false). Esto solo sirve en caso de querer depurar (true, true)
    local procedure OnBeforeCalcularFechaSegundaVacunaF(Rec: Record MAALinPlanVacunacionC01; xRec: Record MAALinPlanVacunacionC01; rlMAAVariosC01: Record MAAVariosC01; var IsHandled: Boolean)
    begin
        //EN UNA PUBLICACIÓN NO PUEDE HABER CÓDIGO, SOLO SE "CONSTRUYE" LA FUNCIÓN
    end;

    [IntegrationEvent(false, false)]
    local procedure OnAfterCalcularFechaSegundaVacunaF(Rec: Record MAALinPlanVacunacionC01; xRec: Record MAALinPlanVacunacionC01; rlMAAVariosC01: Record MAAVariosC01)
    begin

    end;

}
