/// <summary>
/// Report MAAEtiquetaC01 (ID 50104). Informe para hacer etiquetas
/// </summary>
report 55104 "MAAEtiquetaC01"
{
    Caption = 'Etiqueta';
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './layout/Etiqueta.rdlc';
    //UseRequestPage = false;

    dataset
    {
        dataitem("Sales Shipment Line"; "Sales Shipment Line")
        {
            column(DocumentNo_SalesShipmentLine; "Document No.")
            {
            }
            column(No_SalesShipmentLine; "No.")
            {
            }
            column(Description_SalesShipmentLine; Description)
            {
            }

            //Para sacar tantas etiquetas como cantidad tenga un producto
            dataitem(NoEtiquetas; Integer)
            {
                column(Number; Number)
                {

                }

                trigger OnPreDataItem()

                begin
                    NoEtiquetas.SetRange(Number, 1, "Sales Shipment Line".Quantity);
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {

                }
            }
        }
    }
}