/// <summary>
/// PageExtension "MAASalesOrderListC01" (ID 55105) extends Record Sales Order List.
/// </summary>
pageextension 55105 "MAASalesOrderListExtC01" extends "Sales Order List"
{
    actions
    {
        addlast(processing)
        {
            //EJERCICIO 2. Exportar pedidos de venta
            action(MAAExportarPedidosVentaC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar pedidos';
                Image = Export;

                trigger OnAction()
                begin
                    ExportarPedidosVentaF();
                end;
            }

            //EJERCICIO 2. Importar pedidos de venta
            action(MAAImportarPedidosVentaC01)
            {
                ApplicationArea = All;
                Caption = 'Importar pedidos';
                Image = Import;

                trigger OnAction()
                begin
                    ImportarPedidosVentaF();
                end;
            }
        }
    }
    // ------------------------------------------------------------------------------------------------------ //
    /// <summary>
    /// Exporta los pedidos de venta seleccionados por el usuario en la lista de pedidos de venta. 
    /// </summary>
    local procedure ExportarPedidosVentaF()
    var
        rlSalesHeader: Record "Sales Header";  // Record de la tabla de Cabeceras
        rlSalesLine: Record "Sales Line";  // Record de la tabla de Líneas
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary; // Record de la tabla donde tengo el Blob
        xlInStream: Instream;
        clSeparador: Label '*', Locked = true;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5%1%6%1%7%1%8%1%9%1%10%1%11', Locked = true;
        clCadenaLineas: Label 'L%1%2%1%3%1%4%1%5%1%6%1%7%1%8%1%9%1%10', Locked = true;
        xlOutStream: OutStream;
        xlNombreFichero: Text;
    begin
        //Inicializo el OutStream (para exportación)
        TempLMAAConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows);

        //Obtenemos DataSet de cabeceras
        CurrPage.SetSelectionFilter(rlSalesHeader);
        //SetLoadFields(Campos a exportar)

        //Recorremos cabeceras
        if rlSalesHeader.FindSet(false) then begin  //Utilizamos FindSet(false) porque solo queremos leer
            repeat
                xlOutStream.WriteText(StrSubstNo(clCadenaCabecera, clSeparador, rlSalesHeader."Document Type", rlSalesHeader."No.", rlSalesHeader."Bill-to Name", rlSalesHeader."Bill-to Contact", rlSalesHeader."Bill-to Customer No.", rlSalesHeader."Sell-to Customer No.", rlSalesHeader."Bill-to Name", rlSalesHeader."Location Code", rlSalesHeader."Order Date", rlSalesHeader.Status));
                xlOutStream.WriteText(); //Introduzco salto de línea

                //Filtramos lineas
                rlSalesLine.SetRange("Document Type", rlSalesHeader."Document Type");
                rlSalesLine.SetRange("Document No.", rlSalesHeader."No.");
                //SetLoadFields(Campos a exportar)

                //Recorremos lineas
                if rlSalesLine.FindSet(false) then begin
                    repeat
                        xlOutStream.WriteText(StrSubstNo(clCadenaLineas, clSeparador, rlSalesLine."Line No.", rlSalesLine.Type, rlSalesLine."No.", rlSalesLine.Description, rlSalesLine."Location Code", rlSalesLine.Quantity, rlSalesLine."Unit of Measure", rlSalesLine."Unit Price", rlSalesLine."Planned Delivery Date"));
                        xlOutStream.WriteText(); //Introduzco salto de línea
                    until rlSalesLine.Next() = 0;
                end;
            until rlSalesHeader.Next() = 0;
        end;

        //Inicializo el InStream (para importación)
        TempLMAAConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);

        xlNombreFichero := 'PedidosVenta.csv';
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end else begin
            Message('Pedidos de venta exportados correctamente.');
        end;
    end;

    // ------------------------------------------------------------------------------------------------------ //
    /// <summary>
    /// Importa las cabeceras y las líneas de los pedidos de venta pasados por fichero .csv. 
    /// </summary>
    local procedure ImportarPedidosVentaF()
    var
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary; // Record de la tabla donde tengo el Blob
        rlMAALogC01: Record MAALogC01;
        rlSalesHeader: Record "Sales Header";  // Record de la tabla de Cabeceras
        rlSalesLine: Record "Sales Line";  // Record de la tabla de Líneas
        culMAACapturarErroresC01: Codeunit MAACapturarErroresC01;
        elSalesDocumentType: Enum "Sales Document Type";
        xlInStream: Instream;
        xlNum: Integer;
        xlPos: Integer;
        clSeparator: Label '*', Locked = true;
        xlLinea: Text;
        xlContLin: Integer;
    begin
        //Inicializo el InStream (para que coincida mi Encoding en la importación)
        TempLMAAConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);

        if UploadIntoStream('', xlInStream) then begin

            // Limpiamos tabla de Logs
            if not rlMAALogC01.IsEmpty then begin // Si mi tabla tiene Logs de antes, bórralos
                rlMAALogC01.DeleteAll(true);  // Instrucción Bulk
            end;
            // Inicializamos contador
            xlContLin := 0;

            while not xlInStream.EOS do begin // Mientras no lleguemos al final del Stream (EOS)
                xlInStream.ReadText(xlLinea); // Lee el texto de una línea
                xlContLin += 1; // Contador del nº de línea leído

                // Insertar Cabecera o Línea en función del 1er caracter
                case xlLinea.ToUpper() [1] of
                    'C':
                        begin
                            //Insert de las claves principales
                            rlSalesHeader.Init();
                            if Evaluate(elSalesDocumentType, xlLinea.Split(clSeparator).Get(2)) then begin
                                rlSalesHeader.Validate("Document Type", elSalesDocumentType);
                            end;
                            rlSalesHeader.Validate("No.", 'NEW' + xlLinea.Split(clSeparator).Get(3));
                            rlSalesHeader.Insert(true);

                            //Modify del resto de campos
                            for xlPos := 4 to 11 do begin
                                //Capturamos los errores que se produzcan, con una función Try
                                if not culMAACapturarErroresC01.ComprobarCampoCabecerasF(rlSalesHeader, xlLinea, clSeparator, xlPos) then begin
                                    rlMAALogC01.InsertarErrorF(StrSubstNo(('Error en Cabeceras, fila %1, campo %2: ' + GetLastErrorText()), xlContLin, xlPos));
                                end;
                            end;
                            rlSalesHeader.Modify(true);
                        end;

                    'L':
                        begin
                            //Insert de las claves principales
                            rlSalesLine.Init();
                            rlSalesLine.Validate("Document Type", rlSalesHeader."Document Type");
                            rlSalesLine.Validate("Document No.", rlSalesHeader."No.");
                            if Evaluate(xlNum, xlLinea.Split(clSeparator).Get(2)) then begin
                                rlSalesLine.Validate("Line No.", xlNum);
                            end;
                            rlSalesLine.Insert(true);

                            //Modify del resto de campos
                            for xlPos := 3 to 10 do begin
                                //Capturamos los errores que se produzcan, con una función Try
                                if not culMAACapturarErroresC01.ComprobarCampoLineasF(rlSalesLine, xlLinea, clSeparator, xlPos) then begin
                                    rlMAALogC01.InsertarErrorF(StrSubstNo(('Error en Líneas, fila %1, campo %2: ' + GetLastErrorText()), xlContLin, xlPos));
                                end;
                            end;
                            rlSalesLine.Modify(true);
                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end;
            end;
            if not rlMAALogC01.IsEmpty then begin
                Message('Se han producido errores durante el proceso de importación. Por favor, revíselos.');
                //Ejecutamos la página de Logs para que nos muestre los posibles errores capturados
                Page.Run(Page::MAAListaLogsC01);
            end else begin
                Message('Se han importado los pedidos de venta correctamente.');
            end;

        end else begin
            Error('No se ha podido subir el fichero al servidor');
        end;
    end;
}
