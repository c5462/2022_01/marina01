/// <summary>
/// PageExtension "MAACustomerListExtC01" (ID 50102) extends Record Customer List.
/// </summary>
pageextension 55102 MAACustomerListExtC01 extends "Customer List"
{
    actions
    {
        addfirst(General)
        {
            // Probar Codeunit de Variables Globales
            action(MAASwitchVarC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba variables globales a BC - MAA';

                trigger OnAction()
                var
                    //'Extract to Label' para el fichero de traducciones
                    clMensaje: Label 'El valor actual es %1. Lo cambiamos a %2';
                begin
                    Message(clMensaje, cuMAAVariablesGlobalesC01.GetSwitchF(), not cuMAAVariablesGlobalesC01.GetSwitchF());
                    cuMAAVariablesGlobalesC01.SetSwitchF(not cuMAAVariablesGlobalesC01.GetSwitchF());
                end;
            }
            // Probar Codeunit de Captura de Errores
            action(MAAPruebaCuCapturaErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Codeunit Captura de errores MAA';

                trigger OnAction()
                var
                    culCapturarErrores: Codeunit MAACapturarErroresC01;
                begin
                    //Ejecutamos la Codeunit de forma Modal (if ... Run() --> ejecutar y esperar a que el usuario haga algo)
                    if culCapturarErrores.Run() then begin
                        Message('La Codeunit se ha ejecutado correctamente');
                    end else begin
                        Message('No se ha podido ejecutar la Codeunit. Motivo: %1', GetLastErrorText());
                    end;
                end;
            }
            // Probar Función que llama a función Try de Captura de Errores
            action(MAAPruebaFnCapturaErroresC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba Función Captura de errores MAA';

                trigger OnAction()
                var
                    culCapturarErrores: Codeunit MAACapturarErroresC01;
                    i: Integer;
                    xlCodProveedor: Code[20];

                begin
                    xlCodProveedor := '10000';

                    for i := 1 to 5 do begin
                        if culCapturarErrores.FuncionParaCapturarErrorF(xlCodProveedor) then begin
                            Message('Se ha creado el proveedor %1', xlCodProveedor);
                        end;
                        //Para ir incrementando el proveedor de 1 en 1 dentro del bucle
                        xlCodProveedor := IncStr(xlCodProveedor);
                    end;
                end;
            }
        }

        //Botón añadido al final de 'Historial'
        addlast(History)
        {
            action(MAAMuestraFacturasyAbonosC01)
            {
                ApplicationArea = All;
                Caption = 'Muestra Facturas y Abonos MAA';
                Image = ShowChart;

                trigger OnAction()
                begin
                    MostrarFacturasyAbonosF();
                end;
            }
        }

        //Botones añadidos al final de 'Acciones'
        addlast(processing)
        {
            // Utilizar CalcFields mediante exportación de fichero 
            action(MAAExportarRegistroClienteC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar registro cliente';
                Image = ExportContact;

                trigger OnAction()
                begin
                    ExportarRegistroClienteF();
                end;
            }

            // Sumatorio de un campo NO calculado con FindSet() y repeat. Sin problema.
            action(MAASumatorioFindSet01C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio con repeat 01';

                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime();
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No."); // Tengo el nº de cliente en el Rec de la página
                    if rlDetailedCustLedgEntry.FindSet(false) then begin
                        repeat
                            xlTotal += rlDetailedCustLedgEntry.Amount;
                        until rlDetailedCustLedgEntry.Next() = 0
                    end;
                    Message('Total = %1. Ha tardado (%2)', xlTotal, CurrentDateTime() - xlInicio);
                end;
            }

            // Sumatorio de un campo NO calculado con CalcSums(). Aumenta el rendimiento, llamada bulk.
            action(MAASumatorioCalcSums02C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio con CalcSums 01';

                trigger OnAction()
                var
                    rlDetailedCustLedgEntry: Record "Detailed Cust. Ledg. Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime();
                    rlDetailedCustLedgEntry.SetRange("Customer No.", Rec."No."); // Tengo el nº de cliente en el Rec de la página
                    rlDetailedCustLedgEntry.CalcSums(Amount);
                    xlTotal := rlDetailedCustLedgEntry.Amount;
                    Message('Total = %1. Ha tardado (%2)', xlTotal, CurrentDateTime() - xlInicio);
                end;
            }

            // Sumatorio de un campo calculado con FindSet() y repeat. Sin problema. 
            action(MAASumatorioFindSet03C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio con repeat 02';

                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime();
                    rlCustLedgerEntry.SetRange("Customer No.", Rec."No."); // Tengo el nº de cliente en el Rec de la página
                    rlCustLedgerEntry.SetLoadFields(Amount); // Para no traerme toda la tabla, solamente el campo Amount. Solo para cuando vamos a leer un registro, sin hacer modificaciones en SQL
                    rlCustLedgerEntry.SetAutoCalcFields(Amount); // Con esta se gana rendimiento en SQL
                    if rlCustLedgerEntry.FindSet(false) then begin
                        repeat
                            //rlCustLedgerEntry.CalcFields(Amount); // Aquí lo tengo que calcular porque no tengo en la página (antes eran clientes, ahora es otra página)
                            xlTotal += rlCustLedgerEntry.Amount;
                        until rlCustLedgerEntry.Next() = 0
                    end;
                    Message('Total = %1. Ha tardado (%2)', xlTotal, CurrentDateTime() - xlInicio);
                end;
            }

            // Sumatorio de un campo calculado con CalcSums(). NO FUNCIONA.
            action(MAASumatorioCalcSums04C01)
            {
                ApplicationArea = All;
                Caption = 'Sumatorio con CalcSums 02';

                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlTotal: Decimal;
                    xlInicio: DateTime;
                begin
                    xlInicio := CurrentDateTime();
                    rlCustLedgerEntry.SetRange("Customer No.", Rec."No."); // Tengo el nº de cliente en el Rec de la página
                    rlCustLedgerEntry.CalcSums(Amount); //En este caso no funciona, porque estamos haciendo un CalcSums() de un campo calculado, y eso por ahora no se permite
                    xlTotal := rlCustLedgerEntry.Amount;
                    Message('Total = %1. Ha tardado (%2)', xlTotal, CurrentDateTime() - xlInicio);
                end;
            }

            // EXPORTACIÓN DE FICHEROS EN EXCEL. TODO ESTO ESTÁ EN MEMORIA. 
            action(MAAExportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'Exportar Excel';
                Image = Excel;

                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xlFila: Integer;
                    xlCol: Integer;
                begin
                    TempLExcelBuffer.CreateNewBook('Clientes'); //Crea un libro nuevo, con una hoja 'Clientes'

                    //Rellenar las cabeceras de las celdas de excel
                    AsignarCeldaF(1, 1, Rec.FieldCaption("No."), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 2, Rec.FieldCaption(Name), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 3, Rec.FieldCaption("Responsibility Center"), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 4, Rec.FieldCaption("Location Code"), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 5, Rec.FieldCaption("Phone No."), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 6, Rec.FieldCaption(Contact), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 7, Rec.FieldCaption("Balance (LCY)"), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 8, Rec.FieldCaption("Balance Due"), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 9, Rec.FieldCaption("Sales (LCY)"), true, true, TempLExcelBuffer);
                    AsignarCeldaF(1, 10, Rec.FieldCaption("Payments (LCY)"), true, true, TempLExcelBuffer);

                    // Para que traiga los campos calculados
                    rlCustomer.SetAutoCalcFields("Balance (LCY)", "Balance Due", "Sales (LCY)", "Payments (LCY)");
                    // Para que solo me cargue estos campos y no toda la tabla de clientes (evitar el SELECT * en SQL)
                    rlCustomer.LoadFields("No.", Name, "Responsibility Center", "Location Code", "Phone No.", Contact, "Balance (LCY)", "Balance Due", "Sales (LCY)", "Payments (LCY)");

                    //Aplico la selección del usuario de la página a la tabla, y vemos si tienen registros
                    CurrPage.SetSelectionFilter(rlCustomer); // Esta funcionalidad la tienen todas las páginas
                    if rlCustomer.FindSet(false) then begin
                        xlFila := 2; // Inicializamos en fila 2 porque la cabecera es 1
                        xlCol := 1;
                        //Relleno las celdas de excel
                        repeat
                            AsignarCeldaF(xlFila, xlCol, rlCustomer."No.", false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, rlCustomer.Name, false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, rlCustomer."Responsibility Center", false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, rlCustomer."Location Code", false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, rlCustomer."Phone No.", false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, rlCustomer.Contact, false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, Format(rlCustomer."Balance (LCY)"), false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, Format(rlCustomer."Balance Due"), false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, Format(rlCustomer."Sales (LCY)"), false, false, TempLExcelBuffer);
                            xlCol += 1;
                            AsignarCeldaF(xlFila, xlCol, Format(rlCustomer."Payments (LCY)"), false, false, TempLExcelBuffer);

                            xlCol := 1; // Volvemos a poner la columna a 1
                            xlFila += 1; // Vamos incrementando filas
                        until rlCustomer.Next() = 0;
                    end;

                    TempLExcelBuffer.WriteSheet('', '', ''); // Lo preparamos para poder escribir en la hoja
                    TempLExcelBuffer.CloseBook(); //Cerramos nuestra hoja
                    TempLExcelBuffer.OpenExcel(); //Me abre la hoja rellenada para hcer lo que yo quiera con ella
                end;
            }

            // IMPORTACIÓN DE FICHEROS EN EXCEL.
            action(MAAImportarExcelC01)
            {
                ApplicationArea = All;
                Caption = 'Importar Excel';
                Image = Excel;

                trigger OnAction()
                var
                    TempLExcelBuffer: Record "Excel Buffer" temporary;
                    rlCustomer: Record Customer;
                    xlFichero: Text;
                    xlInStream: Instream;
                    xlNombreHoja: Text;
                begin
                    if UploadIntoStream('Seleccione el fichero Excel para importar los datos', '', '', xlFichero, xlInStream) then begin
                        xlNombreHoja := TempLExcelBuffer.SelectSheetsNameStream(xlInStream);

                        if xlNombreHoja = '' then begin
                            Error('Proceso cancelado');
                        end;
                        TempLExcelBuffer.OpenBookStream(xlInStream, xlNombreHoja);
                        TempLExcelBuffer.ReadSheet(); // Rellenamos la tabla temporal Excel Buffer 

                        // Preparamos la tabla para el FindSet()
                        //TempLExcelBuffer.SetCurrentKey("Row No.", "Column No."); // En este caso no hace falta ordenar por la clave
                        TempLExcelBuffer.SetFilter("Row No.", '>%1', 1);
                        TempLExcelBuffer.SetFilter("Column No.", '<%1', 7);

                        if TempLExcelBuffer.FindSet(false) then
                            repeat
                                case TempLExcelBuffer."Column No." of
                                    1:
                                        begin
                                            //Rellenamos la clave y después el resto de campos
                                            rlCustomer.Init();
                                            rlCustomer.Validate("No.", TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Insert(true);
                                        end;
                                    2:
                                        rlCustomer.Validate(Name, TempLExcelBuffer."Cell Value as Text");
                                    3:
                                        rlCustomer.Validate("Responsibility Center", TempLExcelBuffer."Cell Value as Text");
                                    4:
                                        rlCustomer.Validate("Location Code", TempLExcelBuffer."Cell Value as Text");
                                    5:
                                        rlCustomer.Validate("Phone No.", TempLExcelBuffer."Cell Value as Text");
                                    6:
                                        begin
                                            rlCustomer.Validate(Contact, TempLExcelBuffer."Cell Value as Text");
                                            rlCustomer.Modify(true);
                                        end;
                                end;

                            until TempLExcelBuffer.Next() = 0;

                    end else begin
                        Error('No se ha podido cargar el fichero')
                    end;
                end;
            }

            // CREAR PÁGINA DE FILTROS
            action(MAAPaginaFiltrosC01)
            {
                ApplicationArea = All;
                Caption = 'Página de filtros';

                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    rlCustomer: Record Customer;
                    rlSalesPrice: Record "Sales Price";
                    rlVendor: Record Vendor;
                    xlPagFiltros: FilterPageBuilder;
                    clClientes: Label 'Clientes';
                    clMovs: Label 'Movimientos';
                    clPrecios: Label 'Precios';
                begin
                    xlPagFiltros.PageCaption('Seleccione los clientes a procesar');
                    // Esto es como añadir un 'dataitem' en informes
                    xlPagFiltros.AddRecord(clClientes, rlCustomer);
                    // Esto es como añadir una 'column' al dataitem
                    xlPagFiltros.AddField(clClientes, rlCustomer."No.");
                    xlPagFiltros.AddField(clClientes, rlCustomer.MAAFechaUltVacunaC01);

                    xlPagFiltros.AddRecord(clMovs, rlCustLedgerEntry);
                    xlPagFiltros.AddField(clMovs, rlCustLedgerEntry."Posting Date");
                    xlPagFiltros.AddField(clMovs, rlCustLedgerEntry."Document Type");

                    xlPagFiltros.AddRecord(clPrecios, rlSalesPrice);
                    xlPagFiltros.AddField(clPrecios, rlSalesPrice."Item No.");
                    xlPagFiltros.AddField(clPrecios, rlSalesPrice."Starting Date");

                    if xlPagFiltros.RunModal() then begin
                        rlCustomer.SetView(xlPagFiltros.GetView(clClientes, true)); //Aplicamos filtro a la página con SetView. 
                        rlVendor.SetView(xlPagFiltros.GetView(clClientes, true)); //Lo que me seleccione el usuario en clientes lo puedo aplicar también a proveedores de la misma forma, con SetView. Con 'true' me trae los filtros por nombre, con 'false' por nº de campo (pero tienen que ser del mismo tipo de dato)
                        if rlCustomer.FindSet(false) then begin
                            repeat
                            //.....Código.....
                            until rlCustomer.Next() = 0;
                        end;

                        //Con estos podríamos hacer lo mismo
                        Message(xlPagFiltros.GetView(clClientes, true));
                        Message(xlPagFiltros.GetView(clMovs, true));
                        Message(xlPagFiltros.GetView(clPrecios, true));

                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }
        }
    }

    // Función para utilizar en la EXPORTACIÓN DE FICHEROS EN EXCEL
    local procedure AsignarCeldaF(pFila: Integer; pColumna: Integer; pContenido: Text; pNegrita: Boolean; pItalic: Boolean; var TempPExcelBuffer: Record "Excel Buffer" temporary)
    begin
        //Insertamos el registro con 'true' (es de las pocas excepciones, normalmente con tablas temporales es false)
        TempPExcelBuffer.Init();
        TempPExcelBuffer.Validate("Row No.", pFila);
        TempPExcelBuffer.Validate("Column No.", pColumna);
        TempPExcelBuffer.Insert(true);
        // Modify del registro con 'true'
        TempPExcelBuffer.Validate("Cell Value as Text", pContenido);
        TempPExcelBuffer.Validate(Bold, pNegrita);
        TempPExcelBuffer.Validate(Italic, pItalic);
        TempPExcelBuffer.Modify(true);
    end;


    // ----------------------------------------------------------------------------------------//
    //PROCEDIMIENTO QUE COGE EL HIST. DE FACTURAS Y EL HIST. DE ABONOS DE UN CLIENTE Y LOS MUESTRA EN UNA MISMA PÁGINA
    local procedure MostrarFacturasyAbonosF()
    var
        TempLSalesInvoiceHeader: Record "Sales Invoice Header" temporary;
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
        rlSalesCrMemoHeader: Record "Sales Cr.Memo Header";
        pglPostedSalesInvoices: Page "Posted Sales Invoices";
    begin
        //Recorremos las facturas del cliente y las insertamos en la tabla temporal
        rlSalesInvoiceHeader.SetRange("Sell-to Customer No.", Rec."No.");
        //Message(rlSalesInvoiceHeader.GetFilter("Sell-to Customer No.")); // Muestra el filtro que tiene aplicado el campo

        if rlSalesInvoiceHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.Copy(rlSalesInvoiceHeader); //Se puede utilizar Copy porque son idénticas
                TempLSalesInvoiceHeader.Insert(false);

            until rlSalesInvoiceHeader.Next() = 0;
        end;

        //Recorremos los abonos del cliente y las insertamos en la tabla temporal
        rlSalesCrMemoHeader.SetRange("Sell-to Customer No.", Rec."No.");
        //Message(rlSalesCrMemoHeader.GetFilters); // Muestra todos los filtros del objeto

        if rlSalesCrMemoHeader.FindSet(false) then begin
            repeat
                TempLSalesInvoiceHeader.Init();
                TempLSalesInvoiceHeader.TransferFields(rlSalesCrMemoHeader); //Aquí no podemos usar Copy porque son distintas
                TempLSalesInvoiceHeader.Insert(false);

            until rlSalesCrMemoHeader.Next() = 0;
        end;

        //Mostramos la página con facturas + abonos
        pglPostedSalesInvoices.SetRecord(TempLSalesInvoiceHeader);
        pglPostedSalesInvoices.SetTableView(TempLSalesInvoiceHeader);
        Page.Run(0, TempLSalesInvoiceHeader); //Ejecuta la página que tenga por defecto el LookupPageId de TempLSalesInvoiceHeader
        //pglPostedSalesInvoices.Run(); // Hago Run() porque solo quiero mostrar información, no interacción
    end;


    //PROCEDIMIENTO PARA EXPORTAR CÓDIGO, NOMBRE Y SALDO DE UN CLIENTE
    local procedure ExportarRegistroClienteF()
    var
        rlCustomer: Record Customer;
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary; // Record de la tabla donde tengo el Blob
        xlInStream: Instream;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4%', Locked = true;
        clSeparator: Label '*', Locked = true;
        xlOutStream: OutStream;
        xlNombreFichero: Text;
    begin

        TempLMAAConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows); //Inicializo el OutStream (para exportación)

        //Escribir el registro. Equivale a hacer un Get de la clave principal:
        //rlCustomer.Get(Rec."No.");
        rlCustomer := Rec;
        rlCustomer.Find();
        rlCustomer.CalcFields("Balance (LCY)"); // Para que saque el campo calculado y no lo saque a '0'
        xlOutStream.WriteText(StrSubstNo(clCadenaCabecera, clSeparator, rlCustomer."No.", rlCustomer.Name, rlCustomer."Balance (LCY)"));
        xlOutStream.WriteText(); //Introduzco salto de línea

        TempLMAAConfiguracionC01.MiBlob.CreateInStream(xlInStream); //Inicializo el InStream (para importación)

        xlNombreFichero := 'Clientes.csv';
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end;
    end;


    //-------------------- VARIABLES GLOBALES --------------------//
    var
        cuMAAVariablesGlobalesC01: Codeunit "MAAVariablesGlobalesC01";

}
