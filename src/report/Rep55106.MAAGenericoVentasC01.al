/// <summary>
/// Report MAAGenericoVentasC01 (ID 55106). Informe genérico de ventas (cabeceras más líneas).
/// </summary>
report 55106 "MAAGenericoVentasC01"
{
    ApplicationArea = All;
    Caption = 'Informe de Ventas Genérico';
    UsageCategory = Documents;
    RDLCLayout = './layout/GenericoVentas.rdlc';
    DefaultLayout = RDLC;

    dataset
    {
        dataitem(SalesInvoiceHeader; "Sales Invoice Header") //Facturas
        {
            dataitem(SalesInvoiceLine; "Sales Invoice Line")
            {
                DataItemLink = "Document No." = field("No.");

                trigger OnAfterGetRecord()
                begin
                    LinGenericaVentas.Init();
                    LinGenericaVentas.TransferFields(SalesInvoiceLine);
                    LinGenericaVentas.Insert(false);
                end;
            }
            // Con este trigger decimos que si no hemos filtrado nada en facturas salte
            trigger OnPreDataItem()
            begin
                if SalesInvoiceHeader.GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                CabGenericaVentas.Init();
                CabGenericaVentas.TransferFields(SalesInvoiceHeader);
                CabGenericaVentas.Insert(false);

                xCaptionInforme := 'Factura';
            end;
        }

        dataitem(SalesShipmentHeader; "Sales Shipment Header") //Envíos (albaranes)
        {
            dataitem(SalesShipmentLine; "Sales Shipment Line")
            {
                DataItemLink = "Document No." = field("No.");

                trigger OnAfterGetRecord()
                begin
                    LinGenericaVentas.Init();
                    LinGenericaVentas.TransferFields(SalesShipmentLine);
                    LinGenericaVentas.Insert(false);
                end;
            }
            // Con este trigger decimos que si no hemos filtrado nada en albaranes salte
            trigger OnPreDataItem()
            begin
                if SalesShipmentHeader.GetFilters = '' then begin
                    CurrReport.Skip();
                end;
            end;

            trigger OnAfterGetRecord()
            begin
                CabGenericaVentas.Init();
                CabGenericaVentas.TransferFields(SalesShipmentHeader);
                CabGenericaVentas.Insert(false);

                xCaptionInforme := 'Albarán';
            end;
        }

        dataitem(CabGenericaVentas; MAACabGenericaVentasC01) //Genérico cabecera
        {
            column(No_CabGenericaVentas; "No.")
            {
            }
            column(SelltoCustomerNo_CabGenericaVentas; "Sell-to Customer No.")
            {
            }
            column(PostingDate_CabGenericaVentas; "Posting Date")
            {
            }
            column(xCaptionInforme; xCaptionInforme)
            {
            }

            dataitem(LinGenericaVentas; MAALinGenericaVentasC01) //Genérico líneas
            {
                DataItemLink = "Document No." = field("No.");
                column(No_LinGenericaVentas; "No.")
                {
                }
                column(Description_LinGenericaVentas; Description)
                {
                }
                column(Quantity_LinGenericaVentas; Quantity)
                {
                }

                trigger OnAfterGetRecord()
                var

                begin

                end;
            }

            trigger OnAfterGetRecord()
            var

            begin

            end;
        }
    }
    requestpage
    {
        layout
        {
            area(content)
            {
                group(GroupName)
                {
                }
            }
        }
        actions
        {
            area(processing)
            {
            }
        }
    }
    var
        xCaptionInforme: Text;
}
