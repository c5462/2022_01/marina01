/// <summary>
/// Table MAALogC01 (ID 50104). Tabla de registros de eventos. La rellenamos en la Codeunit Lenta.
/// </summary>
table 55104 "MAALogC01"
{
    Caption = 'Tabla Logs MAA';
    DataClassification = SystemMetadata;

    fields
    {
        field(1; IdUnico; Guid)
        {
            Caption = 'Id.';
            DataClassification = SystemMetadata;
        }
        field(2; Mensaje; Text[250])
        {
            Caption = 'Mensaje';
            DataClassification = SystemMetadata;
        }
    }
    keys
    {
        key(PK; IdUnico)
        {
            Clustered = true;
        }
        key(K2; SystemCreatedAt) // Establecemos una segunda Clave (pero que no es principal)
        {

        }
    }

    //Hacemos un OnInsert para que cada vez que llamemos a la tabla de log, se le asigne un primer Guid para que pueda continuar por ahí
    trigger OnInsert()
    begin
        if IsNullGuid(Rec.IdUnico) then begin
            Rec.Validate(IdUnico, CreateGuid());
        end;
    end;

    /// <summary>
    /// Inserta un mensaje con el último error en la tabla de Logs. 
    /// </summary>
    /// <param name="pLastError">Text.</param>
    procedure InsertarErrorF(pLastError: Text)
    begin
        //Insert en tabla MAALogC01 de registro único
        Rec.Init();
        Rec.Validate(IdUnico, CreateGuid());
        Rec.Insert(true);
        //Modify en tabla MAALogC01
        Rec.Validate(Mensaje, pLastError);
        Rec.Modify(true);
    end;

}
