/// <summary>
/// TableExtension "MAACustomerExtC01" (ID 50100) extends Record Customer.
/// </summary>
tableextension 55100 MAACustomerExtC01 extends Customer
{
    fields
    {
        //En las extensiones de Tabla, los campos se empiezan a numerar por el mismo nº de mi objeto Ext. de Tabla, para no interferir con el estándar.
        //Además, el nombre de los campos lleva nuestro prefijo y sufijo, por la misma razón.
        field(55100; MAANoVacunasC01; Integer)
        {
            Caption = 'Nº de Vacunas';
            FieldClass = FlowField;
            CalcFormula = count(MAALinPlanVacunacionC01 where(ClienteVacuna = field("No.")));
            Editable = false;
        }
        field(55101; MAAFechaUltVacunaC01; Date)
        {
            Caption = 'Fecha Última Vacuna';
            FieldClass = FlowField;
            CalcFormula = max(MAALinPlanVacunacionC01.FechaVacuna where(ClienteVacuna = field("No.")));
            Editable = false;
        }
        field(55102; MAATipoVacunaC01; Code[20])
        {
            Caption = 'Tipo de Vacuna';
            DataClassification = CustomerContent;
            //Relacionamos el campo con la tabla MAAVariosC01 para que aparezca el DropDown de los fieldgroups al pincharle
            TableRelation = MAAVariosC01.Codigo where(Tipo = const(Vacuna));

            trigger OnValidate()
            var
                rlMAAVariosC01: Record MAAVariosC01;
            begin
                //Si consigue traerse el registro, comprueba que el valor del campo Bloqueado sea 'false'
                if rlMAAVariosC01.Get(rlMAAVariosC01.Tipo::Vacuna, Rec.MAATipoVacunaC01) then begin
                    rlMAAVariosC01.TestField(Bloqueado, false); //TestField() da un mensaje de error si no se cumple el valor
                end;
            end;
        }

        //Para modificar el comportamiento de los campos de la tabla del estándar
        modify(Name) // Modifica el campo 'Name' del registro que sea (por eso no ponemos Rec.)
        {
            trigger OnAfterValidate()
            var
                culMAAVariablesGlobalesC01: Codeunit MAAVariablesGlobalesC01;
            begin
                culMAAVariablesGlobalesC01.NombreF(Rec.Name); // De este registro (.Rec) pásame el 'Name'
            end;
        }
    }

    //---------------------- SUSCRIPCIONES A EVENTOS DEL ESTÁNDAR ----------------------//

    // Nos suscribimos al OnAfterInsert de la tabla de clientes, para que me inserte un cliente en todas las empresas
    trigger OnAfterInsert()
    begin
        SincronizarRegistrosEmpresasF(xAccion::Insert);
    end;

    // Nos suscribimos al OnAfterModify de la tabla de clientes, para que me traiga todos los datos de un cliente a todas las empresas
    trigger OnAfterModify()
    begin
        SincronizarRegistrosEmpresasF(xAccion::Modify);
    end;

    // Nos suscribimos al OnAfterDelete de la tabla de clientes, para que borre un cliente en todas las empresas
    trigger OnAfterDelete()
    begin
        SincronizarRegistrosEmpresasF(xAccion::Delete);
    end;


    /// <summary>
    /// Procedimiento para realizar las modificaciones de Insert, Modify y Delete en la misma función.
    /// </summary>
    /// <param name="pAccion">Option.</param>
    local procedure SincronizarRegistrosEmpresasF(pAccion: Option)
    var
        rlCompany: Record Company;
        rlCustomer: Record Customer;
    begin
        //Filtrar por empresas distintas a la que estamos
        rlCompany.SetFilter(Name, '<>%1', CompanyName());

        //Recorrer las empresas. Con 'false' porque no queremos modificar
        if rlCompany.FindSet(false) then begin
            repeat
                //Cambiar de empresa la variable local de registro de clientes
                rlCustomer.ChangeCompany(rlCompany.Name);
                //Asignar los valores de cliente a la variable local 
                rlCustomer.Init(); // Puedo hacer aquí el Init porque luego siempre lo copio
                rlCustomer.Copy(Rec);

                case pAccion of
                    xAccion::Insert:
                        rlCustomer.Insert(false); //Sin 'true' para que no cree los contactos

                    xAccion::Modify:
                        rlCustomer.Modify(false); //Para que no entre en bucle le ponemos 'false'

                    xAccion::Delete:
                        rlCustomer.Delete(false);
                end;
            until rlCompany.Next() = 0;
        end;
    end;

    var
        xAccion: Option Insert,Modify,Delete;

    //En una extensión de tabla también podemos definir keys, con las mismas reglas,
    //Prefijo + Nombre + Sufijo y siguiendo la numeración de mi objeto extensión de tabla.
}
