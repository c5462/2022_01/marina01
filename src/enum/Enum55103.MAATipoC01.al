/// <summary>
/// Enum "MAATipoC01" (ID 50103). Objeto Enum para guardar si es Tipo=Vacuna o Tipo=Otros
/// </summary>
enum 55103 MAATipoC01
{
    value(0; Vacuna)
    {
        Caption = 'Vacuna';
    }
    value(999; Otros)
    {
        Caption = ' Otros';
    }
}
