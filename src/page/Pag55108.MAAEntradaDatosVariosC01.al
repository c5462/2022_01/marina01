/// <summary>
/// Page MAAEntradaDatosVariosC01 (ID 55108). Ejercicio de página "comodín". 
/// En esta página se pueden pasar varios datos y recoger varios datos, de forma dinámica (por eso no tiene tabla asociada). 
/// Aquí se ve muy bien la sobrecarga de funciones.
/// </summary>
page 55108 "MAAEntradaDatosVariosC01"
{
    Caption = 'Entrada de Datos Varios MAA';
    PageType = StandardDialog;

    layout
    {
        area(content)
        {
            //Campos tipo Texto
            field(Texto01; aTextos[1])
            {
                ApplicationArea = All;
                Visible = xIsTxtVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 1];
            }
            field(Texto02; aTextos[2])
            {
                ApplicationArea = All;
                Visible = xIsTxtVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 2];
            }
            field(Texto03; aTextos[3])
            {
                ApplicationArea = All;
                Visible = xIsTxtVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 3];
            }
            field(Texto04; aTextos[4])
            {
                ApplicationArea = All;
                Visible = xIsTxtVisible04;
                CaptionClass = mCaptionClass[xTipoDato::Texto, 4];
            }

            //Campos tipo Boolean
            field(Bool01; aBools[1])
            {
                ApplicationArea = All;
                Visible = xIsBoolVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 1];
            }
            field(Bool02; aBools[2])
            {
                ApplicationArea = All;
                Visible = xIsBoolVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 2];
            }
            field(Bool03; aBools[3])
            {
                ApplicationArea = All;
                Visible = xIsBoolVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Booleano, 3];
            }

            //Campos tipo Decimal
            field(Numerico01; aNumericos[1])
            {
                ApplicationArea = All;
                Visible = xIsNumVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 1];
            }
            field(Numerico02; aNumericos[2])
            {
                ApplicationArea = All;
                Visible = xIsNumVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 2];
            }
            field(Numerico03; aNumericos[3])
            {
                ApplicationArea = All;
                Visible = xIsNumVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Numerico, 3];
            }

            //Campos tipo Fecha
            field(Fecha01; aFechas[1])
            {
                ApplicationArea = All;
                Visible = xIsFechaVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 1];
            }

            field(Fecha02; aFechas[2])
            {
                ApplicationArea = All;
                Visible = xIsFechaVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 2];
            }
            field(Fecha03; aFechas[3])
            {
                ApplicationArea = All;
                Visible = xIsFechaVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Fecha, 3];
            }

            //Campos tipo Password
            field(Password01; aPasswords[1])
            {
                ApplicationArea = All;
                Visible = xIsPassVisible01;
                CaptionClass = mCaptionClass[xTipoDato::Password, 1];
                //Propiedad que muestra en la página los típicos puntitos de los Password (pero en SQL se vería)
                ExtendedDatatype = Masked;
            }
            field(Password02; aPasswords[2])
            {
                ApplicationArea = All;
                Visible = xIsPassVisible02;
                CaptionClass = mCaptionClass[xTipoDato::Password, 2];
                ExtendedDatatype = Masked;
            }
            field(Password03; aPasswords[3])
            {
                ApplicationArea = All;
                Visible = xIsPassVisible03;
                CaptionClass = mCaptionClass[xTipoDato::Password, 3];
                ExtendedDatatype = Masked;
            }
        }
    }

    //-------------------- Zona de variables --------------------//
    var
        xIsTxtVisible01: Boolean;
        xIsTxtVisible02: Boolean;
        xIsTxtVisible03: Boolean;
        xIsTxtVisible04: Boolean;
        xIsBoolVisible01: Boolean;
        xIsBoolVisible02: Boolean;
        xIsBoolVisible03: Boolean;
        xIsNumVisible01: Boolean;
        xIsNumVisible02: Boolean;
        xIsNumVisible03: Boolean;
        xIsFechaVisible01: Boolean;
        xIsFechaVisible02: Boolean;
        xIsFechaVisible03: Boolean;
        xIsPassVisible01: Boolean;
        xIsPassVisible02: Boolean;
        xIsPassVisible03: Boolean;
        aBools: array[3] of Boolean; //Muy importante!! Los arrays empiezan en el [1] no en el [0]
        aNumericos: array[3] of Decimal;
        aTextos: array[4] of Text;
        aPasswords: array[3] of Text;
        aFechas: array[3] of Date;
        mCaptionClass: array[5, 10] of Text;
        mPunteros: array[2, 5] of Integer;
        xTipoPuntero: Option Nulo,Pasado,Leido;
        xTipoDato: Option Nulo,Texto,Booleano,Numerico,Fecha,Password;


    //-------------------- Funciones para mostrar el valor de un campo  --------------------//

    /// <summary>
    /// Hace que se muestre un campo tipo Texto, pasando el caption y el valor del campo
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] += 1;
        mCaptionClass[xTipoDato::Texto, mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pCaption;
        aTextos[mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pValorInicial;
        xIsTxtVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 1;
        xIsTxtVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 2;
        xIsTxtVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 3;
        xIsTxtVisible04 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] >= 4;

        //NO PUEDO UTILIZAR UN BUCLE 'for' PORQUE EL BUCLE NACE Y MUERE DENTRO DE LA FUNCIÓN Y YO NECESITO LLAMARLA VARIAS VECES CON DIFERENTES PARÁMETROS DE ENTRADA
        // mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] += 1;
        // for i := mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto] to ArrayLen(aTexto) do begin 
        //      mCaptionClass[xTipoDato::Texto, mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pCaption;
        //      aTexto[mPunteros[xTipoPuntero::Pasado, xTipoDato::Texto]] := pValorInicial;
        // end;
    end;


    /// <summary>
    /// Hace que se muestre un campo tipo Password, pasando el caption, el valor del campo y si es Password (true)
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    /// <param name="pIsPassword">Boolean.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Text; pIsPassword: Boolean)
    begin
        if pIsPassword then begin
            mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] += 1;
            mCaptionClass[xTipoDato::Password, mPunteros[xTipoPuntero::Pasado, xTipoDato::Password]] := pCaption;
            aPasswords[mPunteros[xTipoPuntero::Pasado, xTipoDato::Password]] := pValorInicial;
            xIsPassVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 1;
            xIsPassVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 2;
            xIsPassVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Password] >= 3;

        end else begin
            CampoF(pCaption, pValorInicial);
        end;
    end;


    /// <summary>
    /// Hace que se muestre un campo tipo Booleano, pasando el caption y el valor del campo
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Text.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Boolean)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] += 1;
        mCaptionClass[xTipoDato::Booleano, mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pCaption;
        aBools[mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano]] := pValorInicial;
        xIsBoolVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 1;
        xIsBoolVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 2;
        xIsBoolVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Booleano] >= 3;
    end;


    /// <summary>
    /// Hace que se muestre un campo tipo Numerico Decimal, pasando el caption y el valor del campo
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Decimal.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Decimal)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] += 1;
        mCaptionClass[xTipoDato::Numerico, mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pCaption;
        aNumericos[mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico]] := pValorInicial;
        xIsNumVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 1;
        xIsNumVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 2;
        xIsNumVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Numerico] >= 3;
    end;


    /// <summary>
    /// Hace que se muestre un campo tipo Fecha, pasando el caption y el valor del campo
    /// </summary>
    /// <param name="pCaption">Text.</param>
    /// <param name="pValorInicial">Date.</param>
    procedure CampoF(pCaption: Text; pValorInicial: Date)
    begin
        mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] += 1;
        mCaptionClass[xTipoDato::Fecha, mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pCaption;
        aFechas[mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha]] := pValorInicial;
        xIsFechaVisible01 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 1;
        xIsFechaVisible02 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 2;
        xIsFechaVisible03 := mPunteros[xTipoPuntero::Pasado, xTipoDato::Fecha] >= 3;
    end;


    //-------------------- Funciones para devolver el valor de un campo  --------------------//

    /// <summary>
    /// Devuelve el texto introducido por el usuario.
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(): Text
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Texto] += 1;
        exit(aTextos[mPunteros[xTipoPuntero::Leido, xTipoDato::Texto]]); //Lo puede devolver porque es variable global y lo he rellenado previamente
    end;


    /// <summary>
    /// Devuelve el valor Password introducido por el usuario mediante variable por referencia. 
    /// Se puede utilizar como función (devuelve Text) o como procedimiento (en la var)
    /// </summary>
    /// <param name="pSalida">VAR Text.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Text): Text
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Password] += 1;
        pSalida := aPasswords[mPunteros[xTipoPuntero::Leido, xTipoDato::Password]];
        exit(pSalida);
    end;


    /// <summary>
    /// Devuelve el booleano introducido por el usuario mediante variable por referencia. 
    /// Se puede utilizar como función (devuelve Boolean) o como procedimiento (en la var)
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure CampoF(var pSalida: Boolean): Boolean  //Redundancia de valores devueltos
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano] += 1;
        pSalida := aBools[mPunteros[xTipoPuntero::Leido, xTipoDato::Booleano]];
        exit(pSalida);
    end;


    /// <summary>
    /// Devuelve el valor numérico introducido por el usuario mediante variable por referencia. 
    /// Se puede utilizar como función (devuelve Decimal) o como procedimiento (en la var)
    /// </summary>
    /// <param name="pSalida">VAR Decimal.</param>
    /// <returns>Return value of type Decimal.</returns>
    procedure CampoF(var pSalida: Decimal): Decimal
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Numerico] += 1;
        pSalida := aNumericos[mPunteros[xTipoPuntero::Leido, xTipoDato::Numerico]];
        exit(pSalida);
    end;


    /// <summary>
    /// Devuelve el valor Fecha introducido por el usuario mediante variable por referencia. 
    /// Se puede utilizar como función (devuelve Date) o como procedimiento (en la var)
    /// </summary>
    /// <param name="pSalida">VAR Date.</param>
    /// <returns>Return value of type Date.</returns>
    procedure CampoF(var pSalida: Date): Date
    begin
        mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha] += 1;
        pSalida := aFechas[mPunteros[xTipoPuntero::Leido, xTipoDato::Fecha]];
        exit(pSalida);
    end;

    //------------ Funciones para devolver el valor de un campo indicando el índice ------------//

    /// <summary>
    /// Devuelve el texto introducido por el usuario en la posición pNumCampo de una lista.
    /// </summary>
    /// <param name="pNumCampo">Integer.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(pNumCampo: Integer): Text
    begin
        exit(aTextos[pNumCampo]);
    end;


    /// <summary>
    /// Devuelve el Password introducido por el usuario en la posición pNumCampo de una lista.
    /// </summary>
    /// <param name="pSalida">VAR Text.</param>
    /// <param name="pNumCampo">Integer.</param>
    /// <returns>Return value of type Text.</returns>
    procedure CampoF(var pSalida: Text; pNumCampo: Integer): Text
    begin
        pSalida := aPasswords[pNumCampo];
        exit(pSalida);
    end;


    /// <summary>
    /// Devuelve un booleano introducido por el usuario en la posición pNumCampo de una lista.
    /// </summary>
    /// <param name="pSalida">VAR Boolean.</param>
    /// <param name="pNumCampo">Integer.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure CampoF(var pSalida: Boolean; pNumCampo: Integer): Boolean  //Redundancia de valores devueltos
    begin
        pSalida := aBools[pNumCampo];
        exit(pSalida);
    end;


    /// <summary>
    /// Devuelve el número decimal introducido por el usuario en la posición pNumCampo de una lista.
    /// </summary>
    /// <param name="pSalida">VAR Decimal.</param>
    /// <param name="pNumCampo">Integer.</param>
    /// <returns>Return value of type Decimal.</returns>
    procedure CampoF(var pSalida: Decimal; pNumCampo: Integer): Decimal
    begin
        pSalida := aNumericos[pNumCampo];
        exit(pSalida);
    end;


    /// <summary>
    /// Devuelve la fecha introducida por el usuario en la posición pNumCampo de una lista.
    /// </summary>
    /// <param name="pSalida">VAR Date.</param>
    /// <param name="pNumCampo">Integer.</param>
    /// <returns>Return value of type Date.</returns>
    procedure CampoF(var pSalida: Date; pNumCampo: Integer): Date
    begin
        pSalida := aFechas[pNumCampo];
        exit(pSalida);
    end;

}
