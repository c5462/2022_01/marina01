/// <summary>
/// Page "MAASubPageLinVacunacionC01" (ID 50106). Subpágina para lineas de vacunación.
/// </summary>
page 55106 MAASubPageLinVacunacionC01
{
    Caption = 'Líneas Plan Vacunación MAA';
    PageType = ListPart; //Importante para poder insertar registros en las lineas
    SourceTable = MAALinPlanVacunacionC01;
    UsageCategory = Lists;
    ApplicationArea = All;
    AutoSplitKey = true; //Crea, de manera automática, un nº de linea proporcional entre el registro anterior y el siguiente

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(CodigoLineas; Rec.CodigoLineas)
                {
                    ToolTip = 'Indica el código de un registro, que será igual para todas las líneas.';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false; // Porque siempre va a ser igual que CodigoCabecera
                }
                field(NoLinea; Rec.NoLinea)
                {
                    ToolTip = 'Indica el nº de línea en un mismo código de cabecera.';
                    ApplicationArea = All;
                    Editable = false;
                    Visible = false; // Porque no aporta nada, y ya hemos visto que el AutoSplitKey lo hace bien
                }
                field(ClienteVacuna; Rec.ClienteVacuna)
                {
                    ToolTip = 'Indica el cliente que ha sido vacunado.';
                    ApplicationArea = All;
                }
                field(NombreCliente; Rec.NombreClienteF())
                {
                    Caption = 'Nombre Cliente';
                    ApplicationArea = All;
                }
                field(FechaVacuna; Rec.FechaVacuna)
                {
                    ToolTip = 'Indica la fecha en la que se realizó la vacunación.';
                    ApplicationArea = All;
                }
                field(CodigoVacuna; Rec.CodigoVacuna)
                {
                    ToolTip = 'Indica el código de la Vacuna suministrada.';
                    ApplicationArea = All;
                }
                field(DescripcionVacuna; Rec.DescripcionVariosF(eMAATipoC01::Vacuna, Rec.CodigoVacuna))
                {
                    Caption = 'Descripción Vacuna';
                    ApplicationArea = All;
                }
                field(FechaSegundaVacuna; Rec.FechaSegundaVacuna)
                {
                    ToolTip = 'Indica la fecha de la próxima vacunación.';
                    ApplicationArea = All;
                }
                field(CodigoOtros; Rec.CodigoOtros)
                {
                    ToolTip = 'Indica el código de Otros.';
                    ApplicationArea = All;
                }
                field(DescripcionOtros; Rec.DescripcionVariosF(eMAATipoC01::Otros, Rec.CodigoOtros))
                {
                    Caption = 'Descripción Otros';
                    ApplicationArea = All;
                }
            }
        }
    }
    //Zona de variables globales
    var
        eMAATipoC01: Enum MAATipoC01;


    // Se ejecuta al insertar un nuevo registro, una nueva línea, pero SOLO para esta subpágina
    // Por eso lo conveniente es hacerlo en el trigger OnValidate del campo de la tabla, y así me sirve para todas las páginas que cree.

    // trigger OnNewRecord(BelowxRec: Boolean)
    // var
    //     rlMAACabPlanVacunacionC01: Record MAACabPlanVacunacionC01;
    // begin
    //     if rlMAACabPlanVacunacionC01.Get(Rec.CodigoLineas) then begin
    //         Rec.Validate(FechaVacuna, rlMAACabPlanVacunacionC01.FechaInicioVacunacionPlan);
    //     end
    // end;
}
