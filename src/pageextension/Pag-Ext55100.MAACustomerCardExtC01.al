/// <summary>
/// PageExtension "MAACustomerCardExtC01" (ID 50100) extends Record Customer Card.
/// </summary>
pageextension 55100 MAACustomerCardExtC01 extends "Customer Card"
{
    layout
    {
        addlast(content)
        {
            group(MAAInfoCovidC01)
            {
                Caption = 'Información Covid MAA';

                field(MAANoVacunasC01; Rec.MAANoVacunasC01)
                {
                    ToolTip = 'Especifica el número de vacunas que tiene el cliente';
                    ApplicationArea = All;
                }
                field(MAAFechaUltVacunaC01; Rec.MAAFechaUltVacunaC01)
                {
                    ToolTip = 'Especifica la fecha en la que se puso la última vacuna';
                    ApplicationArea = All;
                }
                field(MAATipoVacunaC01; Rec.MAATipoVacunaC01)
                {
                    ToolTip = 'Especifica el tipo de vacuna suministrada';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        addlast(processing)
        {
            //Creamos un botón en la página con el que asignamos la vacuna bloqueada, con snippets
            action(MAAasignarVacunaBloqueadaC01)
            {
                ApplicationArea = All;
                Caption = '💉Asignamos vacuna bloqueada❌';

                //Con esto probamos que, para asignar un valor a una variable tipo campo de tabla (registro), hay que usar Validate(),
                //para que pase por el trigger OnValidate() de dicho campo y ejecute el código si lo hubiera
                trigger OnAction()
                begin
                    Rec.Validate(MAATipoVacunaC01, 'SPK');
                end;
            }

            action(MAASwitchVarC01)
            {
                ApplicationArea = All;
                Caption = 'Prueba variables globales a BC';

                trigger OnAction()
                var
                    //'Extract to Label' para el fichero de traducciones
                    clMensaje: Label 'El valor actual es %1. Lo cambiamos a %2';
                begin
                    Message(clMensaje, cuMAAVariablesGlobalesC01.GetSwitchF(), not cuMAAVariablesGlobalesC01.GetSwitchF());
                    cuMAAVariablesGlobalesC01.SetSwitchF(not cuMAAVariablesGlobalesC01.GetSwitchF());
                end;
            }

            //EJERCICIO 1. Al pulsar el botón que me clone la ficha de cliente a proveedor
            action(MAAClonarAProveedorC01)
            {
                ApplicationArea = All;
                Caption = 'Clonar a proveedor';
                Image = Vendor;

                trigger OnAction()
                var
                    rlVendor: Record Vendor;
                begin
                    if rlVendor.Get(Rec."No.") then begin
                        if Confirm('El proveedor %1 ya existe. ¿Desea sobreescribirlo?', false, rlVendor."No.") then begin
                            //Como ya existía lo modificamos
                            rlVendor.TransferFields(Rec, true, true);
                            rlVendor.Modify(true);
                            Message('Proveedor modificado correctamente.');
                        end else begin
                            Message('Proceso cancelado por el usuario.');
                        end;
                    end else begin
                        //Como no existía todavía hacemos Init-Insert
                        rlVendor.Init();
                        rlVendor.TransferFields(Rec, true, true);
                        rlVendor.Insert(true);
                        Message('Proveedor creado correctamente.');
                    end;
                end;
            }
        }
    }

    //---------------- Variables globales ----------------//
    var
        cuMAAVariablesGlobalesC01: Codeunit "MAAVariablesGlobalesC01";


    //---------------- Triggers de página ----------------//

    //Me suscribo al OnQueryClosePage de la extensión de página
    trigger OnQueryClosePage(CloseAction: Action): Boolean
    begin
        exit(Confirm('¿Desea salir de esta página?', false));
    end;

}
