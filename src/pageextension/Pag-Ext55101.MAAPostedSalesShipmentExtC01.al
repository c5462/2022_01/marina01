/// <summary>
/// PageExtension MAAPostedSalesShipmentExtC01 (ID 50101) extends Record Posted Sales Shipment.
/// </summary>
pageextension 55101 "MAAPostedSalesShipmentExtC01" extends "Posted Sales Shipment"
{
    actions
    {
        addlast(Reporting)
        {
            action(MAAEtiquetasC01)
            {
                Caption = 'Mostrar Etiqueta';
                ApplicationArea = All;

                trigger OnAction()
                var
                    rlSalesShipmentLine: Record "Sales Shipment Line";
                    replEtiquetas: Report MAAEtiquetaC01;
                begin
                    rlSalesShipmentLine.SetRange("Document No.", Rec."No.");
                    replEtiquetas.SetTableView(rlSalesShipmentLine);
                    replEtiquetas.RunModal();
                    Report.Run(50101, true, true, rlSalesShipmentLine);
                end;
            }
        }
    }
}
