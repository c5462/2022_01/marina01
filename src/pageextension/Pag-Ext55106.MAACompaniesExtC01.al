/// <summary>
/// PageExtension "MAACompaniesExtC01" (ID 55106) extends Record Companies. EJERCICIO 4.
/// </summary>
pageextension 55106 MAACompaniesExtC01 extends Companies
{
    layout
    {
        addlast(Control1)
        {
            field(MAAClientesC01; ClientesF(false))
            {
                ApplicationArea = All;
                Caption = 'Nº Clientes';
                // Para que me haga un DrillDown a los registros de Customer
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    ClientesF(true);
                end;
            }
            field(MAAProveedoresC01; ProveedoresF(false))
            {
                ApplicationArea = All;
                Caption = 'Nº Proveedores';
                // Para que me haga un DrillDown a los registros de Vendor
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    ProveedoresF(true);
                end;
            }
            field(MAACuentasC01; CuentasF(false))
            {
                ApplicationArea = All;
                Caption = 'Nº Cuentas';
                // Para que me haga un DrillDown a los registros de G/L Account
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    CuentasF(true);
                end;
            }
            field(MAAProductosC01; ProductosF(false))
            {
                ApplicationArea = All;
                Caption = 'Nº Productos';
                // Para que me haga un DrillDown a los registros de Item
                DrillDown = true;
                trigger OnDrillDown()
                begin
                    ProductosF(true);
                end;
            }
        }
    }

    actions
    {
        addlast(processing)
        {
            // Realiza una copia de seguridad de todos los registros de la empresa que seleccionemos
            action(MAACrearCopiaSeguridadC01)
            {
                ApplicationArea = All;
                Caption = 'Crear Copia Seguridad';
                Image = Copy;

                trigger OnAction()
                begin
                    CrearCopiaSeguridadF();
                end;
            }

            // Restaura una copia de seguridad previamente exportada
            action(MAARestaurarCopiaSeguridadC01)
            {
                ApplicationArea = All;
                Caption = 'Restaurar Copia Seguridad';
                Image = Import;

                trigger OnAction()
                begin
                    RestaurarCopiaSeguridadF();
                end;
            }
        }
    }
    // ------------------------ EJERCICIO 4. Registros de empresas ------------------------ //
    local procedure ClientesF(pDrillDown: Boolean): Integer
    var
        rlCustomer: Record Customer;
    begin
        exit(DatosTablaF(rlCustomer, pDrillDown));
    end;

    local procedure ProveedoresF(pDrillDown: Boolean): Integer
    var
        rlVendor: Record Vendor;
    begin
        exit(DatosTablaF(rlVendor, pDrillDown));
    end;

    local procedure CuentasF(pDrillDown: Boolean): Integer
    var
        rlGLAccount: Record "G/L Account";
    begin
        exit(DatosTablaF(rlGLAccount, pDrillDown));
    end;

    local procedure ProductosF(pDrillDown: Boolean): Integer
    var
        rlItem: Record Item;
    begin
        exit(DatosTablaF(rlItem, pDrillDown));
    end;


    local procedure DatosTablaF(pRecVariant: Variant; pDrillDown: Boolean): Integer
    var
        rlCompany: Record Company;
        xlRecRef: RecordRef;
    begin
        // Si el parámetro pRecVariant es un record mételo en mi RecordRef
        if pRecVariant.IsRecord then begin
            xlRecRef.GetTable(pRecVariant);
        end;
        // Si el nombre de la empresa del registro seleccionado es distinto a la empresa donde estoy situado (CompanyName), que me cambie a la empresa del registro con ChangeCompany
        if rlCompany.Get(Rec.Name) then begin
            if Rec.Name <> CompanyName then begin
                xlRecRef.ChangeCompany(Rec.Name);
            end;
        end;
        // Si es 'true' abrimos la página. Si es 'false' me cuenta el nº de registros.
        if pDrillDown then begin
            pRecVariant := xlRecRef;
            Page.Run(0, pRecVariant); // Para abrir la página hay que utilizar una variable tipo Record (pRecVariant)
        end else begin
            exit(xlRecRef.Count);
        end;
    end;

    // ----------------- COPIA DE SEGURIDAD DE TODOS LOS REGISTROS DE UNA EMPRESA ----------------- //
    // -------------------------------------------------------------------------------------------- //
    local procedure CrearCopiaSeguridadF()
    var
        rlAllObjWithCaption: Record AllObjWithCaption;
        rlCompany: Record Company;
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary;
        culTypeHelper: Codeunit "Type Helper";
        xlRecRef: RecordRef;
        xlInStream: Instream;
        i: Integer;
        xlOutStream: OutStream;
        xlNombreFichero: Text;
        xlTextBuilder: TextBuilder;
    begin
        // Empresas seleccionadas
        CurrPage.SetSelectionFilter(rlCompany);

        // 1) Bucle de Empresas
        if rlCompany.FindSet(false) then begin
            repeat
                // Escribir en el fichero 
                // Inicio empresa
                xlTextBuilder.Append('InicioEmpresa' + '<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());

                // 2) Bucle de tablas
                rlAllObjWithCaption.SetRange("Object Type", rlAllObjWithCaption."Object Type"::Table);
                rlAllObjWithCaption.SetFilter("Object ID", '%1|%2|%3|%4', Database::"G/L Account", Database::Customer, Database::Vendor, Database::Item);

                if rlAllObjWithCaption.FindSet(false) then begin
                    repeat
                        // Inicio tabla
                        xlTextBuilder.Append('InicioTabla' + '<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());

                        // 3) Bucle registros de cada empresa
                        xlRecRef.Open(rlAllObjWithCaption."Object ID", false, rlCompany.Name);

                        if xlRecRef.FindSet(false) then begin
                            repeat
                                // Inicio Registro
                                xlTextBuilder.Append('InicioRegistro' + '<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());

                                // 4) Bucle de Campos
                                for i := 1 to xlRecRef.FieldCount do begin
                                    // Si el campo es FlowFilter no hagas nada
                                    if xlRecRef.FieldIndex(i).Class <> xlRecRef.FieldIndex(i).Class::FlowFilter then begin
                                        // Si el campo es FlowField calculalo
                                        if xlRecRef.FieldIndex(i).Class = xlRecRef.FieldIndex(i).Class::FlowField then begin
                                            xlRecRef.FieldIndex(i).CalcField();
                                        end;
                                        // Si no tiene datos no lo escribas
                                        if Format(xlRecRef.FieldIndex(i).Value) <> '' then begin
                                            xlTextBuilder.Append(StrSubstNo('<%1>*<%2>', xlRecRef.FieldIndex(i).Name, xlRecRef.FieldIndex(i).Value) + culTypeHelper.CRLFSeparator());
                                        end;
                                    end;
                                end;

                                // Fin Registro
                                xlTextBuilder.Append('FinRegistro' + '<' + Format(xlRecRef.RecordId) + '>' + culTypeHelper.CRLFSeparator());
                            until xlRecRef.Next() = 0;
                        end;
                        // Cerramos los registros 
                        xlRecRef.Close();

                        // Fin tabla
                        xlTextBuilder.Append('FinTabla' + '<' + rlAllObjWithCaption."Object Name" + '>' + culTypeHelper.CRLFSeparator());
                    until rlAllObjWithCaption.Next() = 0;
                end;

                // Fin empresa
                xlTextBuilder.Append('FinEmpresa' + '<' + rlCompany.Name + '>' + culTypeHelper.CRLFSeparator());
            until rlCompany.Next() = 0;
        end;

        // Inicializo el OutStream (para exportación)
        TempLMAAConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows);

        // Escribimos los datos en el OutStream
        xlOutStream.WriteText(xlTextBuilder.ToText());

        //Inicializo el InStream (para importación)
        TempLMAAConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);

        xlNombreFichero := 'CopiaSeguridadEmpresas.csv';
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end else begin
            Message('Copia de seguridad de los registros exportada correctamente.');
        end;
    end;

    // -------------------------------------------------------------------------------------------- //
    local procedure RestaurarCopiaSeguridadF()
    var
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary; // Record de la tabla donde tengo el Blob
        xlRecRef: RecordRef;
        xlInStream: Instream;
        xlNum: Integer;
        xlLinea: Text;
        xlTexto: Text;
    begin
        // 1) Subir fichero de Copia de Seguridad

        //Inicializo el InStream (para que coincida mi Encoding en la importación)
        TempLMAAConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);

        if UploadIntoStream('', xlInStream) then begin
            // 2) Recorrer el fichero
            while not xlInStream.EOS do begin // Mientras no lleguemos al final del fichero (Stream) (EOS)
                xlInStream.ReadText(xlLinea); //Lee el texto de una línea

                case true of
                    // 3) Si es inicio de tabla, abrir el registro
                    CopyStr(xlLinea, 1, 12) = 'InicioTabla<':
                        begin
                            // Pasamos nombre de la tabla, para obtener el número
                            xlRecRef.Open(ObtenerNumTablaF(CopyStr(xlLinea, 13).TrimEnd('>')));
                        end;

                    // 4) Si es inicio de registro, inicializamos el registro
                    CopyStr(xlLinea, 1, 15) = 'InicioRegistro<':
                        begin
                            xlRecRef.Init();
                        end;

                    // 5) Por cada campo, asignar su valor 
                    xlLinea.StartsWith('<'):
                        begin
                            // Obtenemos el nº del campo
                            xlNum := ObtenerNumCampoF(xlRecRef.Number, xlLinea.Split('*').Get(1).TrimStart('<').TrimEnd('>'));
                            // Obtenemos el valor que queremos pasarle
                            xlTexto := xlLinea.Split('*').Get(2).TrimStart('<').TrimEnd('>');

                            // Queremos que solo grabe valores si los campos no son calculados
                            if xlRecRef.Field(xlNum).Class = FieldClass::Normal then begin
                                // Formateamos al formato correcto y guardamos el Valor
                                xlRecRef.Field(xlNum).Value := TipoCampoYValorF(xlRecRef.Field(xlNum), xlTexto);
                            end;
                        end;

                    // 6) Si es fin de registro, lo insertamos
                    CopyStr(xlLinea, 1, 12) = 'FinRegistro<':
                        begin
                            xlRecRef.Insert(false); // Ponemos 'false' porque no queremos realizar los posprocesos, solo queremos exportar los datos de mi fichero
                        end;

                    // 7) Si es fin de tabla, cerrar el registro
                    CopyStr(xlLinea, 1, 9) = 'FinTabla<':
                        begin
                            xlRecRef.Close();
                        end;
                end;
            end;
            Message('Se ha restaurado correctamente la copia de seguridad.');
        end else begin
            Error('No se ha podido subir el fichero al servidor');
        end;
    end;

    /// <summary>
    /// Devuelve el nº de tabla, pasándole el nombre de la propia tabla.
    /// </summary>
    local procedure ObtenerNumTablaF(pNombreTabla: Text): Integer
    var
        rlField: Record Field;
    begin
        rlField.SetRange(TableName, pNombreTabla);
        rlField.SetLoadFields(TableNo);
        // Usamos FindFirst porque no queremos un grupo de registros, solo el primero que encuentre
        if rlField.FindFirst() then begin
            exit(rlField.TableNo);
        end;
    end;

    /// <summary>
    /// Devuelve el nº de campo, pasándole el nº de la tabla a la que pertenece y el nombre del propio campo.
    /// </summary>
    local procedure ObtenerNumCampoF(pNumTabla: Integer; pNombreCampo: Text): Integer
    var
        rlField: Record Field;
    begin
        rlField.SetRange(TableNo, pNumTabla);
        rlField.SetRange(FieldName, pNombreCampo);
        rlField.SetLoadFields("No.");

        if rlField.FindFirst() then begin
            exit(rlField."No.");
        end;
    end;

    /// <summary>
    /// Formatea al formato correcto según el tipo de dato, para importar un valor.
    /// </summary>
    local procedure TipoCampoYValorF(pFieldRef: FieldRef; pValor: Text): Variant
    var
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary;
        TempLCustomer: Record Customer temporary;
        TempLItem: Record Item temporary;
        xlBigInteger: BigInteger;
        xlBool: Boolean;
        xlDate: Date;
        xlDecimal: Decimal;
        i: Integer;
        xlGuid: Guid;
        xlDateTime: DateTime;
        xlTime: Time;
    begin
        case pFieldRef.Type of

            // Hacemos un Evaluate para formatear
            pFieldRef.Type::Integer, pFieldRef.Type::BigInteger:
                begin
                    if (Evaluate(xlBigInteger, pValor)) then begin
                        exit(xlBigInteger);
                    end;
                end;

            pFieldRef.Type::Date:
                begin
                    if (Evaluate(xlDate, pValor)) then begin
                        exit(xlDate);
                    end;
                end;

            pFieldRef.Type::DateTime:
                begin
                    if (Evaluate(xlDateTime, pValor)) then begin
                        exit(xlDateTime);
                    end;
                end;

            pFieldRef.Type::Time:
                begin
                    if (Evaluate(xlTime, pValor)) then begin
                        exit(xlTime);
                    end;
                end;

            pFieldRef.Type::Decimal:
                begin
                    if (Evaluate(xlDecimal, pValor)) then begin
                        exit(xlDecimal);
                    end;
                end;

            pFieldRef.Type::Boolean:
                begin
                    if (Evaluate(xlBool, pValor)) then begin
                        exit(xlBool);
                    end;
                end;

            pFieldRef.Type::Guid:
                begin
                    if (Evaluate(xlGuid, pValor)) then begin
                        exit(xlGuid);
                    end;
                end;

            pFieldRef.Type::Blob:
                begin
                    exit(TempLMAAConfiguracionC01.MiBlob);
                end;

            pFieldRef.Type::Media:
                begin
                    exit(TempLCustomer.Image);
                end;

            pFieldRef.Type::MediaSet:
                begin
                    exit(TempLItem.Picture);
                end;

            // Recorremos los valores del Option y cuando coincida con mi texto, que me devuelva el índice
            pFieldRef.Type::Option:
                begin
                    for i := 1 to pFieldRef.EnumValueCount() do begin
                        if pFieldRef.GetEnumValueCaption(i).ToLower() = pValor.ToLower() then begin
                            exit(i);
                        end;
                    end;
                end;

            // Si es un Texto que me devuelva el valor tal cual
            else
                exit(pValor);
        end;
    end;


    // ------------------------------------------------------------------------------------------------ //
    local procedure EjemploUsoRecordRefF()
    var
        xlRecRef: RecordRef;
        xlFieldRef: FieldRef;
        i: Integer;
    begin
        if xlRecRef.FindSet(false) then begin
            repeat
                for i := 1 to xlRecRef.FieldCount do begin
                    Message('%1', xlRecRef.FieldIndex(i).Value);
                end;

                // 1ª forma: "Doteando"
                xlRecRef.Field(3).Validate(Format(xlRecRef.Field(3).Value).ToUpper());

                // 2ª forma: utilizando FieldRef
                xlFieldRef := xlRecRef.Field(3);
                xlFieldRef.Validate(Format(xlFieldRef.Value).ToUpper());

                xlRecRef.Modify(true);

            until xlRecRef.Next() = 0;
            xlRecRef.Open(Database::Item);
        end;
        //xlRecRef.KeyIndex(1).FieldCount //Cuenta los registros de la clave principal
    end;

    // ---------------------- VARIABLES GLOBALES ---------------------- //
    var
        rRecVariant: RecordRef;
}
