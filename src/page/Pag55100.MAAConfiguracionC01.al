/// <summary>
/// Page MAAConfiguracionC01 (ID 50100). Página de configuración tipo ficha.
/// </summary>
page 55100 "MAAConfiguracionC01"
{
    Caption = 'Configuración de la App 01 del Curso - MAA';
    PageType = Card;
    SourceTable = MAAConfiguracionC01;
    UsageCategory = Administration;
    ApplicationArea = All;
    AboutTitle = 'En esta página se crea la configuración para la primera App del curso Esp. en Business Central';
    AboutText = 'Configure la App de forma correcta. Asegurese de asignar el valor de campo Cliente Web';
    AdditionalSearchTerms = 'Mi primera App';
    //AutoSplitKey = true;
    DeleteAllowed = false; //Importante!! para las páginas de configuración
    InsertAllowed = false; //No permite insertar registros (para pag. de config.)
    ShowFilter = true;
    //Guarda datos que no están en BBDD, para esta página y este usuario (así cuando vuelves a entrar a la página los mantienes). Se suele utilizar en informes.
    SaveValues = true;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodClienteWeb; Rec.CodClienteWeb)
                {
                    ToolTip = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por Web';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Cod. Cliente Web';
                    AboutText = 'Especifique el cliente de BC que se utilizará para los pedidos que entren por Web';
                    Importance = Promoted; // Te promueve arriba de la pestaña el campo
                }
                field(TextoReg; Rec.TextoReg)
                {
                    ToolTip = 'Specifies the value of the Texto Registro field.';
                    ApplicationArea = All;
                    AboutTitle = 'Campo Texto de Registro';
                    AboutText = 'Especifique el texto que pasará a los movimientos de clientes en el registro de movimientos generados por esta App.';
                }
                field(TipoDoc; Rec.TipoDoc)
                {
                    ToolTip = 'Specifies the value of the Tipo de Documento field.';
                    ApplicationArea = All;
                    Importance = Additional;
                }
                field(TipoTabla; Rec.TipoTabla)
                {
                    ToolTip = 'Specifies the value of the Tabla Condicional field.';
                    ApplicationArea = All;
                }
                field(CodTabla; Rec.CodTabla)
                {
                    ToolTip = 'Specifies the value of the CodTabla field.';
                    ApplicationArea = All;
                }

                //Campo que obtiene el nombre según el tipo de tabla seleccionada
                field(NombreTabla; Rec.NombreTablaSelecF(Rec.TipoTabla, Rec.CodTabla))
                {
                    Caption = 'Nombre Selección';
                    ApplicationArea = All;
                    Editable = false; //Aunque las funciones nunca serán editables
                }
                field(ColorFondo; Rec.ColorFondo)
                {
                    Caption = 'Color de Fondo';
                    ApplicationArea = All;
                }
                field(ColorLetra; Rec.ColorLetra)
                {
                    Caption = 'Color de Letra';
                    ApplicationArea = All;
                }
                field(Password; xPassword) //Prueba de password con variable global
                {
                    Caption = 'Password';
                    ApplicationArea = All;
                }
                field(NumIteraciones; xIteraciones) //Para que el usuario pueda introducir las iteraciones se mete aquí 
                {
                    ApplicationArea = All;
                    Caption = 'Nº de iteraciones';
                }
            }
            group(InfInterna)
            {
                Caption = 'Información Interna';
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                }
                field(SystemId; Rec.SystemId)
                {
                    ToolTip = 'Specifies the value of the SystemId field.';
                    ApplicationArea = All;
                    Importance = Additional; // Muestra el campo cuando pulsamos mostrar más
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                }
            }
            group(CamposCalculados)
            {
                Caption = 'Campos Calculados';

                //Obtenemos el nombre del cliente mediante campo calculado. Si cambiamos el cliente no se actualiza en tiempo de ejecución, tendría que refrescar la página. Sin embargo, podemos filtrar y ordenar; a parte de poder pinchar y visualizarlo.
                field(NombreCliente; Rec.NombreCliente)
                {
                    Caption = 'Nombre del Cliente (campo calculado)';
                    ToolTip = 'Specifies the value of the NombreCliente field.';
                    ApplicationArea = All;
                }

                //Obtenemos el nombre del cliente mediante una función. En este caso si se actualiza en tiempo de ejecución, al cambiar de cliente, pero no podemos filtrar ni ordenar por este campo, solamente podemos visualizar (se utiliza mucho para dar una info), a no ser que lo añadamos como otro campo más. 
                field(NombreClienteF; Rec.NombreClienteF(Rec.CodClienteWeb))
                {
                    Caption = 'Nombre del Cliente (función)';
                    ApplicationArea = All;
                }

                //Obtenemos el nº de unidades disponibles aplicando una serie de filtros, mediante campo calculado.
                field(UnidadesDisponibles; Rec.UnidadesDisponibles)
                {
                    ToolTip = 'Campo que indica la cantidad de unidades vendidas, aplicando filtros.';
                    ApplicationArea = All;
                }
            }
        }
    }
    actions
    {
        area(Processing)
        {
            action(Ejemplo01)
            {
                ApplicationArea = All;
                Caption = 'Mensaje de Acción';
                Image = AboutNav;
                trigger OnAction()
                begin
                    Message('Acción de ejemplo pulsada');
                end;
            }

            //Botón que ejecutará mi página comodín MAAEntradaDatosVariosC01
            action(PedirDatos)
            {
                ApplicationArea = All;
                Caption = 'E/S Datos';
                Image = Change;

                trigger OnAction()
                var
                    pglMAAEntradaDatosVariosC01: Page MAAEntradaDatosVariosC01;
                    xlBool: Boolean;
                    xlNum: Decimal;
                    xlFecha: Date;

                begin //Inicializamos datos llamando a nuestra función
                    pglMAAEntradaDatosVariosC01.CampoF('Localidad', 'Ossa de Montiel');
                    pglMAAEntradaDatosVariosC01.CampoF('Provincia', 'Albacete');
                    pglMAAEntradaDatosVariosC01.CampoF('País', '');
                    pglMAAEntradaDatosVariosC01.CampoF('Código Postal', '');

                    pglMAAEntradaDatosVariosC01.CampoF('Cuota 1er trim. pagada', false);
                    pglMAAEntradaDatosVariosC01.CampoF('Cuota 2o trim. pagada', false);
                    pglMAAEntradaDatosVariosC01.CampoF('Cuota 3er trim. pagada', true);

                    pglMAAEntradaDatosVariosC01.CampoF('Importe 01', 5);
                    pglMAAEntradaDatosVariosC01.CampoF('Importe 02', 20.4);
                    pglMAAEntradaDatosVariosC01.CampoF('Importe 03', 21);

                    pglMAAEntradaDatosVariosC01.CampoF('Fecha de registro', 0D);
                    pglMAAEntradaDatosVariosC01.CampoF('Fecha inicial', 0D);
                    pglMAAEntradaDatosVariosC01.CampoF('Fecha final', 0D);

                    //Page.RunModal(Page::MAAEntradaDatosVariosC01); //Es otra forma de ejecutar la página, pero así no le puedo pasar parámetros porque es un método genérico
                    //pglMAAEntradaDatosVariosC01.Run(); //Si uso esta solo puedo mostrar información, no puedo recuperarla (no haría nada al pulsar Aceptar o Cancelar)
                    if pglMAAEntradaDatosVariosC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin //El Action que devuelve depende del tipo de página, por eso como no me lo se, pongo que esté en ese intervalo de valores

                        Message('Localidad introducida: %1', pglMAAEntradaDatosVariosC01.CampoF());
                        Message('Provincia introducida: %1', pglMAAEntradaDatosVariosC01.CampoF());
                        Message('País introducido: %1', pglMAAEntradaDatosVariosC01.CampoF());
                        Message('Código Postal introducido: %1', pglMAAEntradaDatosVariosC01.CampoF());

                        Message('Cuota 1er trim.: %1', pglMAAEntradaDatosVariosC01.CampoF(xlBool)); //Lo llamo como función (return de la función)
                        pglMAAEntradaDatosVariosC01.CampoF(xlBool); //Lo llamo como procedimiento (variable por referencia)
                        Message('Cuota 2o trim.: %1', xlBool);
                        Message('Cuota 3er trim.: %1', pglMAAEntradaDatosVariosC01.CampoF(xlBool)); //Lo vuelvo a llamar como función

                        Message('Valor numérico 1: %1', pglMAAEntradaDatosVariosC01.CampoF(xlNum)); //Utilizamos las 3 como función
                        Message('Valor numérico 2: %1', pglMAAEntradaDatosVariosC01.CampoF(xlNum));
                        Message('Valor numérico 3: %1', pglMAAEntradaDatosVariosC01.CampoF(xlNum));

                        Message('Valor fecha 1: %1', pglMAAEntradaDatosVariosC01.CampoF(xlFecha)); //Utilizamos las 3 como función
                        Message('Valor fecha 2: %1', pglMAAEntradaDatosVariosC01.CampoF(xlFecha));
                        Message('Valor fecha 3: %1', pglMAAEntradaDatosVariosC01.CampoF(xlFecha));

                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }

            action(CambiarPassword)
            {
                ApplicationArea = All;
                Caption = 'Cambiar Password';
                Image = Administration;

                trigger OnAction()
                begin
                    CambiarPasswordF(); //Lo convertimos a función porque queremos que sea [NonDebuggable]
                end;
            }

            action(ExportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Exportar Vacunas';
                Image = Export;

                trigger OnAction()
                begin
                    ExportarVacunasF();
                end;
            }

            action(ImportarVacunas)
            {
                ApplicationArea = All;
                Caption = 'Importar Vacunas';
                Image = Import;

                trigger OnAction()
                begin
                    ImportarVacunasF();
                end;
            }

            action(MostrarClientesAlmacenGris) // Para ver la propiedad de FilterGroup()
            {
                ApplicationArea = All;
                Caption = 'Muestra clientes almacén gris';
                Image = ShowInventoryPeriods;

                trigger OnAction()
                var
                    rlCustomer: Record Customer;
                    rlCompany: Record Company;
                    xlFiltroGrupo: Integer;
                begin
                    //Mostrar lista de empresas de la BBDD y de la seleccionada mostrar sus clientes GRISES
                    if AccionAceptadaF(Page.RunModal(Page::Companies, rlCompany)) then begin

                        rlCustomer.ChangeCompany(rlCompany.Name);

                        xlFiltroGrupo := rlCustomer.FilterGroup();
                        rlCustomer.FilterGroup(xlFiltroGrupo + 20); // Lo cambio de FilterGroup (página de filtros)
                        rlCustomer.SetRange("Location Code", 'GRIS'); // Filtramos que el campo Location Code sea Gris
                        rlCustomer.FilterGroup(xlFiltroGrupo); // Lo dejo en la página 'FilterGroup' que estaba, para que el usuario no pueda eliminar los filtros de la FilterGroup '20' que es donde los he aplicado

                        // Al RunModal hay que pasarle el nº de página y el registro de la tabla asociado
                        if AccionAceptadaF(Page.RunModal(Page::"Customer List", rlCustomer)) then begin
                            Message('Proceso OK');
                        end else begin
                            Error('Proceso cancelado por el usuario');
                        end;
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }

            action(PruebaTexto) // Prueba con Texto puro. Va escribiendo y destruyendo texto anterior en memoria, por lo que es más lento.
            {
                ApplicationArea = All;
                Caption = 'Pruebas con Text puro';

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTexto: Text;
                begin
                    xlInicio := CurrentDateTime;

                    for i := 1 to xIteraciones do begin
                        xlTexto += '.';
                    end;
                    Message('Tiempo invertido en escribir el texto %1: %2', xlTexto, CurrentDateTime - xlInicio);
                end;
            }

            action(PruebaTextBuilder) // Prueba con TextBuilder. Al ser un objeto solo va anexando lo nuevo, por lo que es mucho más rápido.
            {
                ApplicationArea = All;
                Caption = 'Pruebas con TextBuilder';

                trigger OnAction()
                var
                    xlInicio: DateTime;
                    i: Integer;
                    xlTexto: TextBuilder;
                begin
                    xlInicio := CurrentDateTime;

                    for i := 1 to xIteraciones do begin
                        xlTexto.Append('.');
                    end;
                    Message('Tiempo invertido en escribir el texto %1: %2', xlTexto.ToText(), CurrentDateTime - xlInicio);
                end;
            }

            action(ContarClientes) // Contar los clientes que hay en la página movimientos de cliente, usando listas
            {
                ApplicationArea = All;
                Caption = 'Contar clientes';
                Image = Customer;

                trigger OnAction()
                var
                    rlCustLedgerEntry: Record "Cust. Ledger Entry";
                    xlPagFiltros: FilterPageBuilder;
                    clMovs: Label 'Movimientos';
                    xlLista: List of [Code[20]];
                //xlDic: Dictionary of [Code[20], Decimal];
                //xlDic2: Dictionary of [Code[20], List of [Decimal]];
                begin

                    xlPagFiltros.PageCaption('Seleccione los filtros para movimientos de cliente');
                    // Esto es como añadir un 'dataitem' en informes
                    xlPagFiltros.AddRecord(clMovs, rlCustLedgerEntry);
                    // Esto es como añadir una 'column' al dataitem
                    xlPagFiltros.AddField(clMovs, rlCustLedgerEntry."Posting Date");
                    xlPagFiltros.AddField(clMovs, rlCustLedgerEntry."Document Type");

                    if xlPagFiltros.RunModal() then begin

                        //Aplicamos filtro seleccionado con SetView, con 'true' le indicamos que traiga filtros por nombre 
                        rlCustLedgerEntry.SetView(xlPagFiltros.GetView(clMovs, true));
                        // Que solo traiga el campo Número
                        rlCustLedgerEntry.SetLoadFields("Sell-to Customer No.");

                        if rlCustLedgerEntry.FindSet(false) then begin
                            repeat
                                if not xlLista.Contains(rlCustLedgerEntry."Sell-to Customer No.") then begin
                                    xlLista.Add(rlCustLedgerEntry."Sell-to Customer No.");
                                end;
                            until rlCustLedgerEntry.Next() = 0;
                            Message('El número de clientes con movimientos es: %1', xlLista.Count);
                        end else begin
                            Message('No existen registros dentro del filtro %1', rlCustLedgerEntry.GetFilters);
                        end;
                        //Comprobación de que coge los registros que he dicho en los filtros
                        Page.Run(Page::"Customer Ledger Entries", rlCustLedgerEntry);
                    end else begin
                        Error('Proceso cancelado por el usuario');
                    end;
                end;
            }
        }
    }

    //Utilizamos la función creada en la Tabla para insertar un registro si todavía no he creado ninguno
    trigger OnOpenPage()
    begin
        Rec.GetF();
    end;

    //-------------------------- PROCEDURES --------------------------//
    /// <summary>
    /// Para usar en las páginas RunModal()
    /// </summary>
    /// <param name="pAccion">Action.</param>
    /// <returns>Return variable returnValue of type Boolean.</returns>
    procedure AccionAceptadaF(pAccion: Action) return: Boolean
    begin
        // Si pAccion esta en ese intervalo devuelve true en return
        return := pAccion in [Action::LookupOK, Action::Yes, Action::OK];
    end;


    [NonDebuggable]
    local procedure CambiarPasswordF()
    var
        pglMAAEntradaDatosVariosC01: Page MAAEntradaDatosVariosC01;
        xlAux: Text;
    begin
        pglMAAEntradaDatosVariosC01.CampoF('Password Actual', '', true);
        pglMAAEntradaDatosVariosC01.CampoF('Nuevo Password', '', true);
        pglMAAEntradaDatosVariosC01.CampoF('Confirmar Password', '', true);

        //Lanza la página y espera a que el usuario haga algo: ejecuta un código u otro según si le damos a Aceptar o Cancelar
        if pglMAAEntradaDatosVariosC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin

            // 1ª vez que llamo a la función y la comparo con el Password del Isolated Storage de SQL
            if pglMAAEntradaDatosVariosC01.CampoF(xlAux) = Rec.PassWordF() then begin

                // 2ª y 3ª vez que llamo a la función (valores diferentes)
                if pglMAAEntradaDatosVariosC01.CampoF(xlAux) = pglMAAEntradaDatosVariosC01.CampoF(xlAux) then begin
                    Rec.PassWordF(xlAux); // Me llevo el nuevo Password al Isolated Storage de SQL
                    Message('Password cambiado correctamente');
                end else begin
                    Error('Nuevo Password no coincide');
                end;

            end else begin
                Error('Password actual no coincide');
            end;

        end else begin
            Error('Proceso cancelado por el usuario');
        end;
    end;


    //------------------ PROCEDURE PARA EXPORTAR UN FICHERO ------------------//
    local procedure ExportarVacunasF()
    var
        rlMAACabPlanVacunacionC01: Record MAACabPlanVacunacionC01;  // Record de la tabla de Cabeceras
        rlMAALinPlanVacunacionC01: Record MAALinPlanVacunacionC01;  // Record de la tabla de Líneas
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary; // Record de la tabla donde tengo el Blob
        pglMAAListaCabPlanVacunacionC01: Page MAAListaCabPlanVacunacionC01; // Página Lista de Cabeceras, para poder hacer un DataSet
        xlInStream: Instream;
        clSeparator: Label '*', Locked = true;
        clCadenaCabecera: Label 'C%1%2%1%3%1%4%1%5', Locked = true;
        clCadenaLineas: Label 'L%1%2%1%3%1%4%1%5%1%6%1%7%1%8', Locked = true;
        xlOutStream: OutStream;
        xlNombreFichero: Text;
    begin
        Message('Seleccione las vacunas que desea exportar');
        pglMAAListaCabPlanVacunacionC01.LookupMode(true); //Muestra los botones Aceptar/Cancelar (cuando la página no es 'StandardDialog')
        pglMAAListaCabPlanVacunacionC01.SetRecord(rlMAACabPlanVacunacionC01); //Comunico página y Tabla, para poder obtener el registro

        TempLMAAConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows); //Inicializo el OutStream (para exportación)

        if pglMAAListaCabPlanVacunacionC01.RunModal() in [Action::LookupOK, Action::OK, Action::Yes] then begin
            pglMAAListaCabPlanVacunacionC01.GetSelectionFilterF(rlMAACabPlanVacunacionC01); //Obtenemos DataSet de cabeceras

            //Recorremos cabeceras
            if rlMAACabPlanVacunacionC01.FindSet(false) then begin  //Utilizamos FindSet(false) porque solo queremos leer, no modificar (evitamos el bloqueo que se produce con 'true'). Con esta propiedad, solo va una vez a SQL y guarda el dataset en memoria (hay que hacer el 'if' para que no de error si no encuentra registros)
                repeat
                    xlOutStream.WriteText(StrSubstNo(clCadenaCabecera, clSeparator, rlMAACabPlanVacunacionC01.CodigoCabecera, rlMAACabPlanVacunacionC01.Descripcion, rlMAACabPlanVacunacionC01.EmpresaVacunadora, Format(rlMAACabPlanVacunacionC01.FechaInicioVacunacionPlan)));
                    xlOutStream.WriteText(); //Introduzco salto de línea

                    //Recorremos lineas
                    rlMAALinPlanVacunacionC01.SetRange(CodigoLineas, rlMAACabPlanVacunacionC01.CodigoCabecera);
                    if rlMAALinPlanVacunacionC01.FindSet(false) then begin
                        repeat
                            xlOutStream.WriteText(StrSubstNo(clCadenaLineas, clSeparator, rlMAALinPlanVacunacionC01.CodigoLineas, rlMAALinPlanVacunacionC01.NoLinea, rlMAALinPlanVacunacionC01.ClienteVacuna, rlMAALinPlanVacunacionC01.FechaVacuna, rlMAALinPlanVacunacionC01.CodigoVacuna, rlMAALinPlanVacunacionC01.CodigoOtros, rlMAALinPlanVacunacionC01.FechaSegundaVacuna));
                            xlOutStream.WriteText(); //Introduzco salto de línea
                        until rlMAALinPlanVacunacionC01.Next() = 0;
                    end;
                until rlMAACabPlanVacunacionC01.Next() = 0;
            end;
        end else begin
            Error('Proceso cancelado por el usuario');
        end;

        TempLMAAConfiguracionC01.MiBlob.CreateInStream(xlInStream); //Inicializo el InStream (para importación)

        xlNombreFichero := 'Vacunas.csv';
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end;
    end;

    //------------------ PROCEDURE PARA IMPORTAR UN FICHERO ------------------//
    local procedure ImportarVacunasF()
    var
        rlMAACabPlanVacunacionC01: Record MAACabPlanVacunacionC01;  // Record de la tabla de Cabeceras
        rlMAALinPlanVacunacionC01: Record MAALinPlanVacunacionC01;  // Record de la tabla de Líneas
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary; // Record de la tabla donde tengo el Blob
        xlInStream: Instream;
        clSeparator: Label '*', Locked = true;
        xlLinea: Text;
        xlFecha: Date;
        xlNum: Integer;
    begin

        TempLMAAConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows); //Inicializo el InStream (para que coincida mi Encoding en la importación)

        if UploadIntoStream('', xlInStream) then begin
            while not xlInStream.EOS do begin // Mientras no lleguemos al final del Stream (EOS)
                xlInStream.ReadText(xlLinea); //Lee el texto de una línea

                // Insertar Cabecera o Línea en función del 1er caracter
                case true of
                    xlLinea[1] = 'C':
                        //Alta cabecera
                        begin
                            rlMAACabPlanVacunacionC01.Init();
                            rlMAACabPlanVacunacionC01.Validate(CodigoCabecera, xlLinea.Split(clSeparator).Get(2));
                            rlMAACabPlanVacunacionC01.Insert(true);

                            rlMAACabPlanVacunacionC01.Validate(Descripcion, xlLinea.Split(clSeparator).Get(3));
                            rlMAACabPlanVacunacionC01.Validate(EmpresaVacunadora, xlLinea.Split(clSeparator).Get(4));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparator).Get(5)) then begin
                                rlMAACabPlanVacunacionC01.Validate(FechaInicioVacunacionPlan, xlFecha);
                            end;
                            rlMAACabPlanVacunacionC01.Modify(true);
                        end;

                    xlLinea[1] = 'L':
                        //Alta líneas 
                        begin
                            rlMAALinPlanVacunacionC01.Init();
                            rlMAALinPlanVacunacionC01.Validate(CodigoLineas, xlLinea.Split(clSeparator).Get(2));
                            if Evaluate(xlNum, xlLinea.Split(clSeparator).Get(3)) then begin
                                rlMAALinPlanVacunacionC01.Validate(NoLinea, xlNum);
                            end;
                            rlMAALinPlanVacunacionC01.Insert(true);

                            rlMAALinPlanVacunacionC01.Validate(ClienteVacuna, xlLinea.Split(clSeparator).Get(4));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparator).Get(5)) then begin
                                rlMAALinPlanVacunacionC01.Validate(FechaVacuna, xlFecha);
                            end;
                            rlMAALinPlanVacunacionC01.Validate(CodigoVacuna, xlLinea.Split(clSeparator).Get(6));
                            rlMAALinPlanVacunacionC01.Validate(CodigoOtros, xlLinea.Split(clSeparator).Get(7));
                            if Evaluate(xlFecha, xlLinea.Split(clSeparator).Get(8)) then begin
                                rlMAALinPlanVacunacionC01.Validate(FechaSegundaVacuna, xlFecha);
                            end;
                            rlMAALinPlanVacunacionC01.Modify(true);
                        end;
                    else
                        Error('Tipo de línea %1 no reconocido', xlLinea[1]);
                end;
            end;
            Message('Se han importado los planes de vacunación correctamente.');
        end else begin
            Error('No se ha podido subir el fichero al servidor');
        end;
    end;


    //------------------ Variables globales ------------------//
    var
        xPassword: Text;
        xIteraciones: Integer;
}
