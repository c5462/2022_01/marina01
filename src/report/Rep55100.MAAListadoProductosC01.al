/// <summary>
/// Report MAAListadoProductosC01 (ID 50100).
/// </summary>
report 55100 "MAAListadoProductosC01"
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC; //Indicas de que tipo quieres que sea el informe
    RDLCLayout = './layout/ListadoProductos.rdlc';
    AdditionalSearchTerms = 'Listado de Productos Ventas/Compras';
    Caption = 'Listado de Productos Ventas/Compras';

    dataset
    {
        dataitem(Productos; Item) //Nuestro item productos dentro del área del informe
        {
            RequestFilterFields = "No.", "Item Category Code"; //Filtros de página
            column(No_Productos; "No.")
            {
            }
            column(Description_Productos; Description)
            {
            }
            column(ItemCategoryCode_Productos; "Item Category Code")
            {
            }

            dataitem(FacturaVenta; "Sales Invoice Line") //Ventas de mis productos
            {
                // Linkamos el campo de mis facturas de venta con el inmediatamente superior igual, en mi tabla de Items (Productos)
                DataItemLink = "No." = field("No.");
                RequestFilterFields = "Posting Date"; //Filtros de Ventas
                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(DocumentNo_FacturaVenta; "Document No.")
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }

                //Ponemos aquí el cliente porque está al mismo nivel de las ventas
                column(NombreCliente; rCustomer.Name)
                {

                }
                column(NoCliente; rCustomer."No.")
                {

                }

                // Este trigger hace lo mismo que DataItemLink, pero te podrías referir a otro item que no sea el inmediatamente superior, si no a cualquiera
                // trigger OnPreDataItem()
                // begin
                //     FacturaVenta.SetRange("No.",Productos."No.");
                // end;

                trigger OnAfterGetRecord()
                begin
                    if not rCustomer.Get(FacturaVenta."Sell-to Customer No.") then begin
                        Clear(rCustomer);
                    end;
                    // rCustomer.SetRange("No.", FacturaVenta."Sell-to Customer No.");
                    // rCustomer.SetFilter("No.", FacturaVenta."Sell-to Customer No." + '..' + FacturaVenta."Sell-to Customer No.");
                    // rCustomer.FindFirst();
                end;
            }
            dataitem(FacturaCompra; "Purch. Inv. Line") //Compras de mis productos
            {
                // Linkamos el campo de mis facturas de compra con el inmediatamente superior igual, en mi tabla de Items (Productos)
                DataItemLink = "No." = field("No.");
                RequestFilterFields = "Posting Date"; //Filtros de Compras
                column(Quantity_FacturaCompra; Quantity)
                {
                }
                column(DocumentNo_FacturaCompra; "Document No.")
                {
                }
                column(UnitCost_FacturaCompra; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_FacturaCompra; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaCompra; "Posting Date")
                {
                }

                //Ponemos aquí el proveedor porque está al mismo nivel de las compras
                column(NombreProveedor; rVendor.Name)
                {

                }
                column(NoProveedor; rVendor."No.")
                {

                }

                trigger OnAfterGetRecord()
                begin
                    if not rVendor.Get(FacturaCompra."Buy-from Vendor No.") then begin
                        Clear(rVendor);
                    end;
                end;
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {

                }
            }
        }
    }

    //Zona de variables globales
    var
        rCustomer: Record Customer;
        rVendor: Record Vendor;
}