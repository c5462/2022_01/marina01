/// <summary>
/// Report MAAComprasyVentasC01 (ID 55105). Report de Compras a proveedores y ventas a clientes. 
/// </summary>
report 55105 "MAAComprasyVentasC01"
{
    Caption = 'Informe de Compras y Ventas';
    UsageCategory = Documents;
    ApplicationArea = All;
    DefaultLayout = RDLC;
    RDLCLayout = './layout/ListadoComprasyVentas.rdlc';
    AdditionalSearchTerms = 'Listado Compras/Ventas';

    dataset
    {
        dataitem(InfoEmpresa; "Company Information") //Cabeceras con info. de empresa
        {
            column(xCompanyAddr1; xCompanyAddr[1])
            {

            }
            column(xCompanyAddr2; xCompanyAddr[2])
            {

            }
            column(xCompanyAddr3; xCompanyAddr[3])
            {

            }
            column(xCompanyAddr4; xCompanyAddr[4])
            {

            }
            column(xCompanyAddr5; xCompanyAddr[5])
            {

            }
            column(xCompanyAddr6; xCompanyAddr[6])
            {

            }
            column(xCompanyAddr7; xCompanyAddr[7])
            {

            }
            column(xCompanyAddr8; xCompanyAddr[8])
            {

            }
            column(Logo; rCompanyInfo.Picture)
            {

            }
        }
        dataitem(Proveedores; Vendor)
        {
            column(No_Proveedores; "No.")
            {
            }
            column(Name_Proveedores; Name)
            {
            }

            //Dataitem de Compras, dentro de Proveedores
            dataitem(FacturaCompra; "Purch. Inv. Line")
            {
                // Linkamos el campo de mis facturas de compra con el inmediatamente superior igual, en mi tabla de Vendor
                DataItemLink = "Buy-from Vendor No." = field("No.");
                RequestFilterFields = "Posting Date"; //Filtros de Compras

                column(DocumentNo_FacturaCompra; "Document No.")
                {
                }
                column(Quantity_FacturaCompra; Quantity)
                {
                }
                column(UnitCost_FacturaCompra; "Unit Cost")
                {
                }
                column(AmountIncludingVAT_FacturaCompra; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaCompra; "Posting Date")
                {
                }

                //Dataitem de Producto, dentro de Compras
                dataitem(ProductoVendor; Item)
                {
                    DataItemLink = "No." = field("No.");
                    RequestFilterFields = "No.", "Item Category Code"; //Filtros de página

                    column(No_ProductoVendor; "No.")
                    {
                    }
                    column(Description_ProductoVendor; Description)
                    {
                    }
                    column(ItemCategoryCode_ProductoVendor; "Item Category Code")
                    {
                    }
                }
            }

        }
        dataitem(Clientes; Customer)
        {
            column(No_Clientes; "No.")
            {
            }
            column(Name_Clientes; Name)
            {
            }

            //Dataitem de Ventas, dentro de Clientes
            dataitem(FacturaVenta; "Sales Invoice Line")
            {
                // Linkamos el campo de mis facturas de venta con el inmediatamente superior igual, en mi tabla Customer
                DataItemLink = "Sell-to Customer No." = field("No.");
                RequestFilterFields = "Posting Date"; //Filtros de Ventas
                column(Quantity_FacturaVenta; Quantity)
                {
                }
                column(DocumentNo_FacturaVenta; "Document No.")
                {
                }
                column(UnitPrice_FacturaVenta; "Unit Price")
                {
                }
                column(AmountIncludingVAT_FacturaVenta; "Amount Including VAT")
                {
                }
                column(PostingDate_FacturaVenta; "Posting Date")
                {
                }

                //Dataitem de Producto, dentro de Ventas
                dataitem(ProductoCustomer; Item)
                {
                    DataItemLink = "No." = field("No.");
                    RequestFilterFields = "No.", "Item Category Code"; //Filtros de página

                    column(No_ProductoCustomer; "No.")
                    {
                    }
                    column(Description_ProductoCustomer; Description)
                    {
                    }
                    column(ItemCategoryCode_ProductoCustomer; "Item Category Code")
                    {
                    }
                }
            }
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {

                }
            }
        }
    }

    //Triggers globales del informe
    trigger OnPreReport()
    begin
        rCompanyInfo.Get(); //Cargamos la tabla de info. de empresa
        rCompanyInfo.CalcFields(Picture);
        cuFormatAddress.Company(xCompanyAddr, rCompanyInfo);
    end;

    //Zona de variables globales
    var
        rCompanyInfo: Record "Company Information";
        cuFormatAddress: Codeunit "Format Address";
        xCompanyAddr: array[8] of Text;
}