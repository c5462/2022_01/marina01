/// <summary>
/// Page "MAACardVariosC01" (ID 50102).
/// </summary>
page 55102 MAACardVariosC01
{
    UsageCategory = None;
    Caption = 'Ficha Vacunas y Otros MAA';
    PageType = Card;
    SourceTable = MAAVariosC01;
    AdditionalSearchTerms = 'Curso 01';
    AboutTitle = 'Ficha Tabla Varios';
    AboutText = 'Se usará para editar una tabla donde tenemos varios tipos de registros de Vacunas Covid';

    layout
    {
        area(content)
        {
            group(General)
            {
                field(Tipo; Rec.Tipo)
                {
                    ToolTip = 'Indica si el tipo de registro es Vacuna u Otros.';
                    ApplicationArea = All;
                    AboutTitle = 'Tipo de registro';
                    AboutText = 'Indica si el tipo de registro es Vacuna u Otros.';
                }
                field(Codigo; Rec.Codigo)
                {
                    ToolTip = 'Indica el Código del registro.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Descripción del registro Vacuna u Otros.';
                    ApplicationArea = All;
                }
                field(PeriodoSegundaDosis; Rec.PeriodoSegundaDosis)
                {
                    ToolTip = 'Indica el tiempo entre la primera y la segunda dosis de una vacuna.';
                    ApplicationArea = All;
                }
                field(Bloqueado; Rec.Bloqueado)
                {
                    ToolTip = 'Indica si el registro está o no bloqueado.';
                    ApplicationArea = All;
                }
            }
            group(Informacion)
            {
                Editable = false;
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(SystemCreatedBy; Rec.SystemCreatedBy)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
                field(SystemModifiedAt; Rec.SystemModifiedAt)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedAt field.';
                    ApplicationArea = All;
                }
                field(SystemModifiedBy; Rec.SystemModifiedBy)
                {
                    ToolTip = 'Specifies the value of the SystemModifiedBy field.';
                    ApplicationArea = All;
                    Visible = false;
                }
            }
        }
    }
}
