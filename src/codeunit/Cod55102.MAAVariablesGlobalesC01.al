/// <summary>
/// Codeunit MAAFuncionesC01 (ID 50102). Codeunit de variables globales y funciones para usar en todo BC.
/// </summary>
codeunit 55102 "MAAVariablesGlobalesC01"
{
    SingleInstance = true; //MUY IMPORTANTE!!!! Para que mantenga el valor de las variables globales entre objetos diferentes. No hay que abusar de esto, usar solo en este tipo de Codeunit y en la Codeunit suscriptora

    procedure SetSwitchF(pValor: Boolean)
    begin
        xSwitch := pValor;
    end;


    procedure GetSwitchF() return: Boolean
    begin
        return := xSwitch;
    end;


    /// <summary>
    /// Guarda el nombre del cliente a nivel global de BC.
    /// </summary>
    /// <param name="pName">Text[100].</param>
    procedure NombreF(pName: Text[100])
    begin
        xName := pName;
    end;


    /// <summary>
    /// Devuelve el nombre del cliente guardado globalmente.
    /// </summary>
    /// <returns>Return value of type Text.</returns>
    procedure NombreF(): Text
    begin
        exit(xName);
    end;

    //-------- VARIABLES GLOBALES A TODO BC (porque es SingleInstance) --------//
    var
        xSwitch: Boolean;
        xName: Text;
}