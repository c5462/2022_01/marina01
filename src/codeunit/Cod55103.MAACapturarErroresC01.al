/// <summary>
/// Codeunit "MAACapturarErroresC01" (ID 50103). Sirve para capturar los errores que van apareciendo y almacenarlos en memoria. Para ello, hay que ejecutar la Codeunit desde otro objeto, de forma Modal (if ... Run()), o utilizando una función Try.
/// </summary>
codeunit 55103 MAACapturarErroresC01
{
    //Se lanza al ejecutar la Codeunit con un Run() en otro objeto
    trigger OnRun()
    var
        rlCustomer: Record Customer;
        clCodCte: Label 'AAA', Locked = true; //Para que no traduzca

    begin
        if not rlCustomer.Get(clCodCte) then begin

            rlCustomer.Init(); //Inicializamos el registro

            // 1) Hacemos un validate de la PK y su insert
            rlCustomer.Validate("No.", clCodCte);
            rlCustomer.Insert(true); //Para que ejecute los trigger del OnInsert()

            // 2) Hacemos un validate del resto de campos y su modify
            rlCustomer.Validate(Name, 'Nombre prueba');
            rlCustomer.Validate(Address, 'Calle de prueba');
            rlCustomer."VAT Registration No." := 'XXXX'; //Para que no verifique el CIF si está en otro cliente, solo se utiliza := en estos casos especiales
            rlCustomer.Validate("Payment Method Code", 'MAA');
            rlCustomer.Modify(true); //Para que ejecute los trigger del OnModify()
        end;
        Message('Datos del cliente %1', rlCustomer);
    end;

    // -------- TRY FUNCTION DE EJEMPLO ------------------------------------------------------------------//
    /// <summary>
    /// Función que me captura un error utilizando a su vez una función Try.
    /// </summary>
    /// <param name="pCodProveedor">Code[20].</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure FuncionParaCapturarErrorF(pCodProveedor: Code[20]): Boolean
    var
        rlVendor: Record Vendor;

    begin
        if not rlVendor.Get(pCodProveedor) then begin

            rlVendor.Init(); //Inicializamos el registro
            rlVendor.Validate("No.", pCodProveedor);
            rlVendor.Insert(true); //Para que ejecute los trigger del OnInsert() de la tabla Vendor (posproceso)

            //Aqui es donde utilizamos la función Try
            if AsignarDatosProveedorF(rlVendor) then begin
                rlVendor.Modify(true); //Para que ejecute los trigger del OnModify() de la tabla Vendor (posproceso)
            end else begin
                Message('Error al intentar asignar los datos: %1', GetLastErrorText());
            end;
            exit(true);
        end;
        Message('El proveedor %1 no se ha creado porque ya existe', pCodProveedor);
    end;

    //Intenta ejecutarla y si no deja, dime el error. Las funciones 'Try' devuelven true/false.
    //EN UNA FUNCIÓN 'TRY' NO SE PUEDEN REALIZAR MODIFICACIONES EN BBDD, eso se hace en la función de arriba.
    [TryFunction]
    local procedure AsignarDatosProveedorF(var rlVendor: Record Vendor) // Por referencia, para que al salir se mantengan los cambios en mi tabla
    begin
        rlVendor.Validate(Name, 'Nombre prueba');
        rlVendor.Validate(Address, 'Calle de prueba');
        rlVendor."VAT Registration No." := 'XXXX';
        //rlVendor.Validate("Payment Method Code", 'MAA');
    end;

    // -------- TRY FUNCTIONS EJERCICIO 3 ----------------------------------------------------------------//

    [TryFunction]
    procedure ComprobarCampoCabecerasF(var pSalesHeader: Record "Sales Header"; pLinea: Text; pSeparator: Text; pPosicion: Integer)
    var
        elSalesDocumentStatus: Enum "Sales Document Status";
        xlFecha: Date;
    begin
        case pPosicion of
            4:
                pSalesHeader.Validate("Bill-to Name", pLinea.Split(pSeparator).Get(pPosicion));
            5:
                pSalesHeader.Validate("Bill-to Contact", pLinea.Split(pSeparator).Get(pPosicion));
            6:
                pSalesHeader.Validate("Bill-to Customer No.", pLinea.Split(pSeparator).Get(pPosicion));
            7:
                pSalesHeader.Validate("Sell-to Customer No.", pLinea.Split(pSeparator).Get(pPosicion));
            8:
                pSalesHeader.Validate("Bill-to Name", pLinea.Split(pSeparator).Get(pPosicion));
            9:
                pSalesHeader.Validate("Location Code", pLinea.Split(pSeparator).Get(pPosicion));
            10:
                begin
                    if Evaluate(xlFecha, pLinea.Split(pSeparator).Get(pPosicion)) then begin
                        pSalesHeader.Validate("Order Date", xlFecha);
                    end;
                end;
            11:
                begin
                    if Evaluate(elSalesDocumentStatus, pLinea.Split(pSeparator).Get(pPosicion)) then begin
                        pSalesHeader.Validate(Status, elSalesDocumentStatus);
                    end;
                end;
        end;
    end;


    [TryFunction]
    procedure ComprobarCampoLineasF(var pSalesLine: Record "Sales Line"; pLinea: Text; pSeparator: Text; pPosicion: Integer)
    var
        elSalesLineType: Enum "Sales Line Type";
        xlFecha: Date;
        xlDecimal: Decimal;
    begin
        case pPosicion of
            3:
                begin
                    if Evaluate(elSalesLineType, pLinea.Split(pSeparator).Get(pPosicion)) then begin
                        pSalesLine.Validate(Type, elSalesLineType);
                    end;
                end;
            4:
                pSalesLine.Validate("No.", pLinea.Split(pSeparator).Get(pPosicion));
            5:
                pSalesLine.Validate(Description, pLinea.Split(pSeparator).Get(pPosicion));
            6:
                pSalesLine.Validate("Location Code", pLinea.Split(pSeparator).Get(pPosicion));
            7:
                begin
                    if Evaluate(xlDecimal, pLinea.Split(pSeparator).Get(pPosicion)) then begin
                        pSalesLine.Validate(Quantity, xlDecimal);
                    end;
                end;
            8:
                pSalesLine.Validate("Unit of Measure", pLinea.Split(pSeparator).Get(pPosicion));
            9:
                begin
                    if Evaluate(xlDecimal, pLinea.Split(pSeparator).Get(pPosicion)) then begin
                        pSalesLine.Validate("Unit Price", xlDecimal);
                    end;
                end;
            10:
                begin
                    if Evaluate(xlFecha, pLinea.Split(pSeparator).Get(pPosicion)) then begin
                        pSalesLine.Validate("Planned Delivery Date", xlFecha);
                    end;
                end;
        end;
    end;
    // --------------------------------------------------------------------------------------------------//
}
