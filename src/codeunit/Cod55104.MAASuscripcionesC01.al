/// <summary>
/// Codeunit "MAASuscripcionesC01" (ID 50104). Codeunit para suscribirse a eventos que no son del estándar.
/// </summary>
codeunit 55104 MAASuscripcionesC01
{
    SingleInstance = true;

    // Los dos últimos parámetros [false, false] permiten que de un error si el usuario no tiene licencia o permisos para este objeto. Siempre false.
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnBeforePostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnBeforePostSalesDocF(var SalesHeader: Record "Sales Header")
    begin
        SalesHeader.TestField("External Document No."); // Verifica que este dato esté relleno antes de registrar
    end;

    // Nos suscribimos al evento 'después de registrar factura de venta'
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Sales-Post", 'OnAfterPostSalesDoc', '', false, false)]
    local procedure CodeunitSalesPostOnAfterPostSalesDocF(var SalesHeader: Record "Sales Header")
    var
        culMAAFuncionesAppC01: Codeunit MAAFuncionesAppC01;
    begin
        culMAAFuncionesAppC01.VerificarTipoVacunaF(SalesHeader);
    end;


    // Suscripción a una tabla que es del estándar pero no puedo extender (es de System Application), donde URL1 es el campo al que me suscribo
    [EventSubscriber(ObjectType::Table, Database::"Record Link", 'OnAfterValidateEvent', 'URL1', false, false)]
    local procedure DatabaseRecordLinkOnAfterValidateEventURL1F(CurrFieldNo: Integer; var Rec: Record "Record Link"; var xRec: Record "Record Link")
    begin

    end;

    // Suscripción al evento OnAfterPostPurchaseDoc (cuando registro la factura de compra)
    [EventSubscriber(ObjectType::Codeunit, Codeunit::"Purch.-Post", 'OnAfterPostPurchaseDoc', '', false, false)]
    local procedure CodeunitPurchPostOnAfterPostPurchaseDocF(PurchInvHdrNo: Code[20])
    var
        culMAAFuncionesAppC01: Codeunit MAAFuncionesAppC01;
    begin
        // Lo pasamos una función porque una Codeunit 'SingleInstance' tiene que tener el código lo más reducido posible, ya que lo carga en memoria
        culMAAFuncionesAppC01.MensajeFacCompraF(PurchInvHdrNo);
    end;
}