/// <summary>
/// Page "MAAConfiguracionListC01" (ID 50103). Página de configuración tipo lista, para poder mostrar el icono de filtros.
/// La hemos creado para poder ver la utilidad de un campo calculado con filtros. 
/// </summary>
page 55103 MAAConfiguracionListC01
{
    ApplicationArea = All;
    Caption = 'Configuración de la App 01 del curso - Lista MAA';
    PageType = List;
    SourceTable = MAAConfiguracionC01;
    UsageCategory = Lists;
    ShowFilter = true; //Muestra el icono de filtrar

    layout
    {
        area(content)
        {
            repeater(General)
            {
                //Solo nos interesa ver el de unidades disponibles, que es el que tiene los filtros
                field(UnidadesDisponibles; Rec.UnidadesDisponibles)
                {
                    ToolTip = 'Campo que indica la cantidad de unidades vendidas, aplicando filtros.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
