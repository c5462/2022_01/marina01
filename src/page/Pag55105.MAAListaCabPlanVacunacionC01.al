/// <summary>
/// Page "MAAListaCabPlanVacunacionC01" (ID 50105). Lista para las cabeceras.
/// </summary>
page 55105 MAAListaCabPlanVacunacionC01
{
    ApplicationArea = All;
    Caption = 'Lista Cab. Plan de Vacunación MAA';
    PageType = List;
    SourceTable = MAACabPlanVacunacionC01;
    UsageCategory = Lists;
    CardPageId = MAAFichaCabPlanVacunacionC01;
    Editable = false;
    InsertAllowed = false;
    DeleteAllowed = false;
    ModifyAllowed = false;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Codigo; Rec.CodigoCabecera)
                {
                    ToolTip = 'Specifies the value of the Código field.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Specifies the value of the Descripción field.';
                    ApplicationArea = All;
                }
                field(FechaInicioVacunacionPlan; Rec.FechaInicioVacunacionPlan)
                {
                    ToolTip = 'Specifies the value of the Fecha Inicio Planificada Vacunación field.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Specifies the value of the Empresa Vacunadora field.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF())
                {
                    Caption = 'Nombre Empresa Vacunadora';
                    ToolTip = 'Aquí aparecerá el nombre del proveedor que realizará la vacunación.';
                    ApplicationArea = All;
                }
            }
        }
    }

    /// <summary>
    /// Devuelve un DataSet de Registros de la variable Tabla pasada por referencia.
    /// </summary>
    /// <param name="prMAACabPlanVacunacionC01">VAR Record MAACabPlanVacunacionC01.</param>
    /// 'var prMAACabPlanVacunacionC01' = 'SourceTable' de la página.
    procedure GetSelectionFilterF(var prMAACabPlanVacunacionC01: Record MAACabPlanVacunacionC01)
    begin
        CurrPage.SetSelectionFilter(prMAACabPlanVacunacionC01); //Propiedad del CurrPage, obtengo el DataSet
    end;
}
