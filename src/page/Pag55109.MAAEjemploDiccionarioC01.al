/// <summary>
/// Página que sirve de ejemplo para ver el tema de los Diccionarios.
/// </summary>
page 55109 "MAAEjemploDiccionarioC01"
{
    ApplicationArea = All;
    Caption = 'Ejemplo Diccionario';
    PageType = List;
    SourceTable = Integer; // Tabla que no existe en SQL, es una tabla virtual tipo Integer (también hay de 'fechas', de 'files' para On-Premise, etc.)
    UsageCategory = Lists;

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(Num; Rec.Number) // La tabla virtual 'Integer' solo tiene este campo, que son números Integer
                {
                    ApplicationArea = All;
                    Caption = 'Nº';
                }
                field(Campo01; ValorCeldaF(Rec.Number, 1)) //Voy obteniendo cada uno de los campos
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 01';
                }
                field(Campo02; ValorCeldaF(Rec.Number, 2))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 02';
                }
                field(Campo03; ValorCeldaF(Rec.Number, 3))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 03';
                }
                field(Campo04; ValorCeldaF(Rec.Number, 4))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 04';
                }
                field(Campo05; ValorCeldaF(Rec.Number, 5))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 05';
                }
                field(Campo06; ValorCeldaF(Rec.Number, 6))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 06';
                }
                field(Campo07; ValorCeldaF(Rec.Number, 7))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 07';
                }
                field(Campo08; ValorCeldaF(Rec.Number, 8))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 08';
                }
                field(Campo09; ValorCeldaF(Rec.Number, 9))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 09';
                }
                field(Campo10; ValorCeldaF(Rec.Number, 10))
                {
                    ApplicationArea = All;
                    Caption = 'Valor celda 10';
                }
            }
        }
    }
    // --------------------------------------------------------------------------------//
    var
        //Matriz de filas 'integer' y columnas 'list of decimal'
        xDiccionario: Dictionary of [Integer, List of [Decimal]];

    // --------------------------------------------------------------------------------//
    trigger OnOpenPage()
    var
        xlFila: Integer;
        xlColumna: Integer;
        xlNumeros: List of [Decimal]; //Para usarla en la segunda dimensión de mi diccionario
    begin
        for xlFila := 1 to 10 do begin
            Clear(xlNumeros);
            for xlColumna := 1 to 10 do begin
                xlNumeros.Add(Random(99999));
            end;
            xDiccionario.Add(xlFila, xlNumeros); //Relleno la variable Diccionario (Integer, {List of Decimal})
        end;
        Rec.SetRange(Number, 1, 10); // Filtra para que me coja del 1 al 10 en la tabla virtual 'Integer'
        EjemploNotificacionesF();
    end;


    local procedure ValorCeldaF(pFila: Integer; pColumna: Integer): Decimal
    begin
        exit(xDiccionario.Get(pFila).Get(pColumna)); //Obtengo los valores de la variable Diccionario que ya he rellenado arriba
    end;

    // ----------------------------- NOTIFICACIONES ----------------------------- //
    local procedure EjemploNotificacionesF()
    var
        xlNotificacion: Notification;
        clMsg: Label 'Carga %1 finalizada';
        i: Integer;
    begin
        for i := 1 to 3 do begin
            xlNotificacion.Id(CreateGuid()); //Asigna un Guid por notificación, para que aparezcan las 3
            xlNotificacion.Message(StrSubstNo(clMsg, i));
            xlNotificacion.Send(); //La enviamos
        end;

        xlNotificacion.Id(CreateGuid());
        xlNotificacion.Message('El cliente no tiene ninguna vacuna');
        //Añadimos una acción a la notificación
        xlNotificacion.AddAction('Abrir lista de clientes', Codeunit::MAAFuncionesAppC01, 'AbrirListaClientesF');
        //Pasamos datos a la notificación
        xlNotificacion.SetData('CodCliente', '10000');
        //La enviamos
        xlNotificacion.Send();
    end;

}
