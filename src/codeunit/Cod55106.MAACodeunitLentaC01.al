/// <summary>
/// Codeunit "MAACodeunitLentaC01" (ID 50106). Codeunit para ejecución de procesos lentos.
/// </summary>
codeunit 55106 "MAACodeunitLentaC01"
{
    trigger OnRun()
    begin
        // Modificar algo en SQL
        EjecutarTareaF();
    end;


    local procedure EjecutarTareaF()
    var
        xlEjecutar: Boolean;
    begin
        xlEjecutar := true;
        // Siempre que vayamos a mostrar una ventana de información en una tarea en segundo plano hay que poner el GuiAllowed (mi tarea principal tiene que tener GUI)
        if GuiAllowed then begin
            xlEjecutar := Confirm('¿Desea ejecutar tarea en segundo plano?')
        end;
        // Yes
        if xlEjecutar then begin
            InsertarLogF('Tarea iniciada');
            Commit(); //No usar casi nunca, solo cuando no queda más remedio
            Message('En proceso');
            Sleep(10000);
            InsertarLogF('Tarea finalizada');
        end;
    end;


    local procedure InsertarLogF(pMensaje: Text)
    var
        rlMAALogC01: Record MAALogC01; // Variable tipo registro de la tabla de eventos
    begin
        //Insert en tabla MAALogC01 de registro único
        rlMAALogC01.Init();
        rlMAALogC01.Insert(true);
        //Modify en tabla MAALogC01
        rlMAALogC01.Validate(Mensaje, pMensaje);
        rlMAALogC01.Modify(true);
    end;
}
