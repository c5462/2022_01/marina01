/// <summary>
/// Codeunit "MAAFuncionesAppC01" (ID 50105). Para meter funciones que necesitemos. 
/// </summary>
codeunit 55105 "MAAFuncionesAppC01"
{
    /// <summary>
    /// Devuelve un mensaje con el nº de factura que se ha registrado. Función usada en la Codeunit suscriptora.
    /// </summary>
    /// <param name="PurchInvHdrNo">Code[20].</param>
    procedure MensajeFacCompraF(PurchInvHdrNo: Code[20])
    begin
        if PurchInvHdrNo <> '' then begin
            Message('El usuario %1 ha creado la factura %2', UserId(), PurchInvHdrNo);
        end;
    end;

    procedure FuncionEjemplo01F(pCodCliente: Code[20]; pParam2: Integer; var prCustomer: Record Customer) return: Text
    var

    begin

    end;


    procedure EjemploVariablesReferenciaF(var pTexto: Text)
    begin
        pTexto := 'Tecon Servicios'; // Cambio el valor
    end;


    procedure EjemploVarObjetosReferenciaF(pTexto: TextBuilder)
    begin
        pTexto.Append('Tecon Servicios'); // Concateno el valor
    end;


    procedure BingoF()
    var
        mlNumeros: array[20] of Text; //Muy importante!! Los arrays empiezan en el [1] no en el [0]
        i: Integer;
        xlNumElementosArray: Integer;
        xlRandomObtenido: Integer;
    begin
        for i := 1 to ArrayLen(mlNumeros) do begin
            mlNumeros[i] := Format(i);
        end;

        xlNumElementosArray := ArrayLen(mlNumeros);

        repeat
            xlRandomObtenido := Random(xlNumElementosArray);
            Message('La bola que ha salido es %1', mlNumeros[xlRandomObtenido]);
            mlNumeros[xlRandomObtenido] := '';
            xlNumElementosArray := CompressArray(mlNumeros);

        until xlNumElementosArray <= 0;
        Message('Ya han salido todas las bolas');
    end;

    //-------------------------------------------------------------------------------------------------------//

    /// <summary>
    /// Procedimiento para interacción con notificaciones. Abre la lista de clientes. 
    /// </summary>
    /// <param name="pNotificacion">Notification.</param>
    procedure AbrirListaClientesF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodCliente';
    begin
        // La notificación trae el dato del nº de cliente, por lo que puedo hacer un SetRange para filtrar por ese campo en la tabla Customer
        if pNotificacion.HasData(clNombreDato) then begin
            rlCustomer.SetRange("No.", pNotificacion.GetData(clNombreDato));
        end;
        Page.Run(Page::"Customer List", rlCustomer);
    end;

    /// <summary>
    /// Procedimiento para interacción con notificaciones. Abre la ficha de cliente. 
    /// </summary>
    /// <param name="pNotificacion">Notification.</param>
    procedure AbrirFichaClienteF(pNotificacion: Notification)
    var
        rlCustomer: Record Customer;
        clNombreDato: Label 'CodCliente';
    begin
        // La notificación trae el dato del nº de cliente, por lo que hago un Get para poder abrir su ficha en concreto
        if pNotificacion.HasData(clNombreDato) then begin
            if rlCustomer.Get(pNotificacion.GetData(clNombreDato)) then begin
                Page.Run(Page::"Customer Card", rlCustomer);
            end;
        end;
    end;

    /// <summary>
    /// Procedimiento usado en la Codeunit de suscripciones. Verifica si el cliente al que estamos facturando tiene introducido el tipo de vacuna.
    /// </summary>
    /// <param name="SalesHeader">Record "Sales Header".</param>
    procedure VerificarTipoVacunaF(SalesHeader: Record "Sales Header");
    var
        rlCustomer: Record Customer;
        rlSalesLine: Record "Sales Line";
        xlNotificacion: Notification;
        clMsgTipoVacuna: Label 'El cliente %1 no tiene tipo de vacuna especificado. Por favor, introdúzcalo.';
        clMsgLineaBaja: Label 'La línea %1 del documento %2, con nº %3, con importe %4, es inferior a 100';

    begin
        //Rellenamos el registro con Get
        if rlCustomer.Get(SalesHeader."Sell-to Customer No.") then begin

            // 1) Verificar que el cliente tiene tipo de vacuna introducido
            if rlCustomer.MAATipoVacunaC01 = '' then begin
                xlNotificacion.Id(CreateGuid());
                xlNotificacion.Message(StrSubstNo(clMsgTipoVacuna, rlCustomer."No."));
                //Interactuamos con la notificación
                xlNotificacion.AddAction('Abrir ficha de cliente', Codeunit::MAAFuncionesAppC01, 'AbrirFichaClienteF');
                xlNotificacion.SetData('CodCliente', rlCustomer."No."); // Le pasamos el Nº de cliente como dato
                xlNotificacion.Send(); //La enviamos
            end;

            // 2) Verificar si las lineas superan los 100 € (Amount)
            rlSalesLine.SetRange("Document No.", SalesHeader."No.");
            rlSalesLine.SetRange("Document Type", SalesHeader."Document Type");
            rlSalesLine.SetFilter(Amount, '<%1', 100);
            rlSalesLine.SetLoadFields("Line No.", "Document No.", Amount); //Quitamos tráfico en SQL

            if rlSalesLine.FindSet(false) then begin
                repeat
                    Clear(xlNotificacion); //Para que no me muestre la notificación anterior
                    xlNotificacion.Id(CreateGuid());
                    xlNotificacion.Message(StrSubstNo(clMsgLineaBaja, rlSalesLine."Line No.", rlSalesLine."Document Type", rlSalesLine."Document No.", rlSalesLine.Amount));
                    xlNotificacion.Send(); //La enviamos
                until rlSalesLine.Next() = 0;
            end;
        end;
    end;

    //-------------------------------------------------------------------------------------------------------//
    /// <summary>
    /// Devuelve el último nº de línea de ventas
    /// </summary>
    /// <param name="pRec">Record "Sales Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimoNumLineaF(pRec: Record "Sales Line"): Integer
    var
        rlRec: Record "Sales Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");

        //Encuentra el último registro de una tabla, basándose en la clave principal y un filtro
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end
    end;

    /// <summary>
    /// Devuelve el último nº de línea de compras
    /// </summary>
    /// <param name="pRec">Record "Purchase Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimoNumLineaF(pRec: Record "Purchase Line"): Integer
    var
        rlRec: Record "Purchase Line";
    begin
        rlRec.SetCurrentKey("Document Type", "Document No.", "Line No.");
        rlRec.SetRange("Document Type", pRec."Document Type");
        rlRec.SetRange("Document No.", pRec."Document No.");

        //Encuentra el último registro de una tabla, basándose en la clave principal y un filtro
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end
    end;


    /// <summary>
    /// Devuelve el último nº de línea de diario general
    /// </summary>
    /// <param name="pRec">Record "Gen. Journal Line".</param>
    /// <returns>Return value of type Integer.</returns>
    procedure UltimoNumLineaF(pRec: Record "Gen. Journal Line"): Integer
    var
        rlRec: Record "Gen. Journal Line";
    begin
        rlRec.SetCurrentKey("Journal Template Name", "Journal Batch Name", "Line No.");
        rlRec.SetRange("Journal Template Name", pRec."Journal Template Name");
        rlRec.SetRange("Journal Batch Name", pRec."Journal Batch Name");

        //Encuentra el último registro de una tabla, basándose en la clave principal y un filtro
        if rlRec.FindLast() then begin
            exit(rlRec."Line No.");
        end
    end;


    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text)

    begin
        EjemploSobrecargaF(p1, p2, p3, false); //Llamamos a una sobrecarga posterior, con el nuevo parámetro, para no romper las Apps que utilicen esta
    end;


    procedure EjemploSobrecargaF(p1: Integer; p2: Decimal; p3: Text; p4: Boolean)
    var
        xlNuevoNum: Decimal;
    begin
        p1 := 1;
        p2 := 2.5;
        p3 := 'Ejemplo sobrecarga';

        if p4 then begin
            xlNuevoNum := p1 * p2;
        end;
    end;


    procedure UsoFuncionEjemploF()
    begin
        EjemploSobrecargaF(10, 10.5, 'Modificación sobrecarga');
    end;


    //----------------------- TEORÍA CON LAS REGLAS DE SINTAXIS -------------------------//
    /// <summary>
    /// SintaxisF. Función general para conocer las reglas de sintaxis. No utilizar, solo es teoría.
    /// </summary>
    procedure SintaxisF()
    var
        rlCustomer: Record Customer;
        xBool: Boolean;
        x: Decimal;
        Ventana: Dialog;
        i: Integer;
        z: Integer;
        xlTexto: Text;
        xlLong: Integer;
        xlPos: Integer;
        xlCampos: List of [Text];
        xlCampo: Text;
        clSeparador: Label '·', Locked = true; // Para que no traduzca
        xlFecha: Date;
        aArray: Array[4] of Text;
    begin
        //Asignaciones
        i := 1;
        i := i + 10;
        i += 10;
        i += 1; //Para incrementar 1

        //Concatenación de textos
        xlTexto := 'Marina';
        xlTexto := xlTexto + 'Alfaro';
        xlTexto += 'Algaba';
        xlTexto := StrSubstNo('%1 %2 %3', 'Marina', 'Alfaro', 'Algaba');

        //Operadores
        i := Round(x + z, 1);    //Sin decimales (precisión 1), redondea a enteros
        i := Round(x + z, 2);    //Números pares (múltiplos de 2)
        i := Round(x + z, 0.01); //Precisión 0.01 (redondea a 2 decimales)
        i := Round(x + z, 0.5);  //Precisión 0.5 (redondea a 1 decimal, de medio en medio)
        i := Round(x + z, 0.5, '<'); //Redondea al valor cercano más bajo, utilizando la precisión 0.5
        i := Round(x + z, 0.1, '>'); //Redondea al valor cercano más alto, utilizando la precisión 0.1
        i := 9 div 2; //Divide 9/2 y se queda con la parte entera (i = 4)
        i := 9 mod 2; //Divide 9/2 y se queda con el resto (i = 1)
        i := Abs(-9); //Valor absoluto

        //Operadores lógicos 
        xBool := not true;  //Negación
        xBool := false and true; //And
        xBool := false or true;  //Or
        xBool := false xor true; //Xor (esta se comporta igual que los interruptores de la luz, uno u otro pero los dos a la vez no)

        //Operadores relacionales
        xBool := 1 < 2; //true
        xBool := 1 > 2; //false
        xBool := 1 <= 2; //true
        xBool := 1 >= 2; //false
        xBool := 1 = 2; //false
        xBool := 1 <> 2; //true
        xBool := 1 in [1, 'B', 2, 'y']; //Devuelve true si '1' está entre esos valores
        xBool := 'a' in ['a' .. 'z']; //Devuelve true si 'a' está en ese rango de valores

        //FUNCIONES MUY USADAS
        Message('Proceso finalizado');
        Message('El cliente %1 no tiene saldo pendiente de pago.', rlCustomer.Name);
        Message('Estamos en el año %1%2%1%1', 2, 0); //Estamos en el año 2022

        Error('Proceso cancelado por el usuario'); //Hace rollback (finaliza el proceso y sale sin terminar la transacción)
        Error('Proceso cancelado por el usuario %1', UserId);

        //Esto es útil para cambiar la funcionalidad de un botón del estándar, por ejemplo, nos suscribimos al OnBeforeAction() (antes de pulsar el botón),
        //hacemos nuestro proceso, lo grabamos, y salimos con el error en blanco (rollback), sin que llegue a ejecutar lo del estándar
        //No lanza error ni mensaje pero como es un error para el proceso (rollback)
        Commit();
        Error('');

        xBool := Confirm('¿Confirma que desea registrar la factura %1?', false, 'FAC001'); //Si no pongo nada por defecto es true, pero es mejor poner false, para que por defecto si el usuario no lee y le da a Intro esté marcado el 'no'

        //Construye un menú: pregunta al usuario y le da una serie de opciones
        i := StrMenu('Enviar, Facturar, Enviar y facturar', 3, '¿Como desea registrar?'); //Por defecto se marca el 3 (Enviar y facturar)
        case i of
            0:
                Error('Proceso cancelado por el usuario'); //El valor 0 lo reserva para 'Cancelar'
            1:
                Message('Pedido enviado'); //Enviar pedido
            2:
                Message('Pedido facturado'); //Facturar pedido
            3:
                Message('Pedido enviado y facturado'); //Enviar y facturar pedido
        end;

        //Siempre que vayamos a mostrar cosas al usuario se usa el GuiAllowed, por si lo lanza un proceso en seguno plano que no de error.
        //Las ventanas se utilizaban para interactuar con el usuario (ahora ya no se puede)
        if GuiAllowed then begin
            //Lo primero que hay que hacer es abrir la ventana, si no da error al usar Update()
            Ventana.Open('Procesando bucle i #1#####\' +
                         'Procesando bucle x #2#####'); //Se le ponen bastantes '#' para que coja el texto
        end;

        for i := 1 to 1000 do begin
            if GuiAllowed then begin
                Ventana.Update(1, i); //Que vaya actualizando el control '1' con valor 'i'
            end;
            for x := 1 to 1000 do begin
                if GuiAllowed then begin
                    Ventana.Update(2, x); //Que vaya actualizando el control '2' con valor 'x'
                end;
            end;
        end;

        if GuiAllowed then begin
            Ventana.Close();
        end;


        //FUNCIONES DE CADENAS DE TEXTO 
        xlLong := MaxStrLen(xlTexto); // Longitud máxima que puede almacenar una cadena
        xlLong := MaxStrLen('ABC');   // Devolverá un 3
        xlLong := StrLen('ABCD   ');  // Devolvería un 4 (no cuenta los espacios)
        xlLong := ArrayLen(aArray);   // Devuelve la longitud de un array

        xlTexto := CopyStr('ABC', 2, 1); // Devuelve una subcadena, desde la posición 2, con 1 caracter (B)
        xlTexto := CopyStr('ABC', 2);    // Devuelve una subcadena, desde la posición 2 hasta el final (BC)
        xlTexto := xlTexto.Substring(2); // Usando xlTexto como objeto. Es lo mismo que CopyStr.

        xlPos := StrPos('Elefante', 'e'); // Devuelve la primera ocurrencia del substring dentro del string. Devolvería un 3.
        xlPos := xlTexto.IndexOf('e');    // Usando xlTexto como objeto. Lo mismo que StrPos.

        xlTexto := UpperCase(xlTexto); // Convierte el texto a mayúsculas
        xlTexto := xlTexto.ToUpper();  // Usando xlTexto como objeto. Lo mismo que UpperCase.
        xlTexto := LowerCase(xlTexto); // Convierte el texto a minúsculas
        xlTexto := xlTexto.ToLower();  // Usando xlTexto como objeto. Lo mismo que LowerCase.

        xlTexto := xlTexto.TrimEnd('\') + '\'; // Borra la barra '\' del final (si lo encuentra) y añade '\'. Así te aseguras de que la ruta siempre tiene ese caracter. 
        xlTexto := xlTexto.TrimStart('\').TrimEnd('\').Trim() + '\'; //Borra '\' del principio y del final, y los espacios. Se encadena texto sobre texto.

        //SEPARADORES DE TEXTO
        xlTexto := '123,456,Tecon Servicios, s.l.,789';
        Message(SelectStr(4, xlTexto)); // con SelectStr el separador es la coma, por lo que devolvería s.l.

        // Con Split puedes utilizar el separador que le digas tu, y te lo convierte a lista de textos
        // Con .Get(i) le decimos que coja el elemento 'i' de la lista
        xlTexto := '123·456·Tecon Servicios, s.l.·789';
        Message(xlTexto.Split(clSeparador).Get(1)); // 123
        Message(xlTexto.Split(clSeparador).Get(2)); // 456
        Message(xlTexto.Split(clSeparador).Get(3)); // Tecon Servicios, s.l
        Message(xlTexto.Split(clSeparador).Get(4)); // 789

        xlCampos := xlTexto.Split(clSeparador); // Igual que lo de arriba pero utilizando una variable tipo lista de textos
        Message(xlCampos.Get(4)); // 789

        // Recorre cada xlTexto de la lista xlCampos (esto es para lenguaje .al) sin tamaños, es dinámico
        foreach xlTexto in xlCampos do begin
        end;

        foreach xlCampo in xlTexto.Split(clSeparador) do begin // lo mismo pero sin crear variable tipo lista
        end;

        // Evaluate() convierte una fecha en un texto (lo contrario a hacer Format();) 
        xlTexto := '21-05-2022';
        Evaluate(xlFecha, ConvertStr(xlTexto, '-', '/')); // ConvertStr me cambia los caracteres '-' por '\', para que BC los reconozca
        Evaluate(xlFecha, xlTexto.Replace('-', '/').Replace(',', '.')); // Usando xlTexto como objeto. Lo mismo que ConvertStr.

        // DelChr borra caracteres de una cadena dada, diciendo donde y cual
        xlTexto := 'Elefante';
        Message(DelChr(xlTexto, '<', 'E')); // lefante (borra al pincipio del todo)
        Message(DelChr(xlTexto, '>', 'e')); // Elefant (borra al final del todo)
        Message(DelChr(xlTexto, '=', 'e')); // Elfante (borra entre medias)
        Message(DelChr(xlTexto, '<=>', 'e')); // lfant (lo borra al pincipio, en medio, y al final)

        xlPos := Power(3, 2); // 3 elevado a 2
        xlTexto := UserId();  // Nombre del usuario que se ha loggeado
        xlTexto := CompanyName(); // Nombre de la empresa en la que estoy 
        Today; // Día en el que estoy
        WorkDate(); // Fecha de trabajo
        Time; // Hora 

    end;


    /// <summary>
    /// Devuelve una subcadena de 'pNumChar' caracteres a la izquierda de pCadena.
    /// </summary>
    /// <param name="pCadena">Text.</param>
    /// <param name="pNumChar">Integer.</param>
    /// <returns>Return value of type Text.</returns>
    procedure LeftF(pCadena: Text; pNumChar: Integer): Text
    begin
        // ABCDEFGH --- 3 --> 'ABC'
        if StrLen(pCadena) < pNumChar then begin
            exit(CopyStr(pCadena, 1, pNumChar));

        end else begin
            Error('Índice fuera de los límites de la cadena')
        end;

    end;

    /// <summary>
    /// Devuelve una subcadena de 'pNumChar' caracteres a la derecha de pCadena.
    /// </summary>
    /// <param name="pCadena">Text.</param>
    /// <param name="pNumChar">Integer.</param>
    /// <returns>Return value of type Text.</returns>
    procedure RightF(pCadena: Text; pNumChar: Integer): Text
    begin
        // ABCDEFGH --- 3 --> 'FGH'
        exit(CopyStr(pCadena, (StrLen(pCadena) - pNumChar) + 1));
    end;

    //------------------------- EJEMPLOS DE COMO USAR GET -------------------------//
    procedure GetNumClienteF(pCodCliente: Code[20]): Record Customer
    var
        rlCustomer: Record Customer;
    begin
        if rlCustomer.Get(pCodCliente) then begin
            exit(rlCustomer);

        end else begin
            Error('El cliente indicado no existe');
        end;
    end;

    procedure GetVariosF(pDimensionId: Integer; pDimensionCode: Code[20])
    var
        rlCustomer: Record Customer;
        rlCustomer2: Record Customer;
        rlSalesLine: Record "Sales Line";
        rlSalesLine2: Record "Sales Line";
        rlDimensionSetEntry: Record "Dimension Set Entry";
        rlCompanyInformation: Record "Company Information";
    begin
        Message('%1', rlSalesLine.Get(rlSalesLine."Document Type"::Order, '101005', 10000));
        Message('El valor de la dimensión es: %1', rlDimensionSetEntry.Get(pDimensionId, pDimensionCode));
        Message('%1', rlCompanyInformation.Get()); // Tabla de registro único

        //Truco para utilizar Find(); en lugar de Get(); para traer registros
        rlCustomer.SetFilter(Name, '%1|%2', '*A*', '*B*');
        if rlCustomer.FindSet() then begin
            repeat
                rlCustomer2 := rlCustomer; //Hago un traspaso para no perder el puntero del FindSet()
                rlCustomer2.Find();
                rlCustomer2.Validate(Name, DelChr(rlCustomer.Name, '<=>', 'AB'));
                rlCustomer2.Modify(true); //'true' porque no es tabla temporal
            until rlCustomer.Next() = 0;
        end;

        rlSalesLine.SetFilter(Description, '%1|%2', '*A*', '*B*');
        if rlSalesLine.FindSet() then begin
            repeat
                rlSalesLine2 := rlSalesLine; //Le paso los campos a rlSalesLine2
                rlSalesLine2.Find(); // Solo se puede utilizar Find() en este caso, equivale a lo siguiente:
                rlSalesLine2.get(rlSalesLine."Document Type", rlSalesLine."Document No.", rlSalesLine."Line No."); //Me ahorro pasarle todos los segmentos de la PK
            // Modificaciones...
            until rlSalesLine.Next() = 0;
        end;
    end;

    //------------------------- EJEMPLOS DE COMO USAR STREAMS -------------------------//
    /// <summary>
    /// Función de ejemplo de como hacer Streams, para reutilizar código a futuro.
    /// </summary>
    procedure StreamsF()
    var
        // Una tabla 'temporary' no guarda datos, no se comunica con SQL. 
        TempLMAAConfiguracionC01: Record MAAConfiguracionC01 temporary; // En este caso no se utiliza 'Validate()' ni 'true' en los trigger de registro.
        xlInStream: InStream;
        xlOutStream: OutStream;
        xlLinea: Text;
        xlNombreFichero: Text;

    begin
        //Inicializamos el canal OutStream en memoria
        TempLMAAConfiguracionC01.MiBlob.CreateOutStream(xlOutStream, TextEncoding::Windows);
        xlOutStream.WriteText('MAA');

        //Inicializamos el canal InStream en memoria
        TempLMAAConfiguracionC01.MiBlob.CreateInStream(xlInStream, TextEncoding::Windows);
        xlInStream.ReadText(xlLinea);

        // Le podemos indicar nombre y extensión del fichero
        xlNombreFichero := 'Pedido.csv';

        // Para exportación
        if not DownloadFromStream(xlInStream, '', '', '', xlNombreFichero) then begin
            Error('El fichero no se ha podido descargar');
        end;

        //Para importación
        if UploadIntoStream('', xlInStream) then begin
            while not xlInStream.EOS do begin // Mientras no lleguemos al final del Stream (EOS)
                //.......
            end;
            Message('Se han importado los datos correctamente.');
        end else begin
            Error('No se ha podido subir el fichero al servidor');
        end;
    end;

    /// <summary>
    /// EJERCICIO 5. Obtener si un código determinado está dentro de un filtro.
    /// </summary>
    /// <param name="pCode">Code[20].</param>
    /// <param name="pFiltro">Text.</param>
    /// <returns>Return value of type Boolean.</returns>
    procedure CumpleFiltroF(pCode: Code[20]; pFiltro: Text): Boolean
    var
        TempLCustomer: Record Customer temporary;
    begin
        TempLCustomer.Init();
        TempLCustomer."No." := pCode;
        TempLCustomer.Insert(false);

        TempLCustomer.SetFilter("No.", pFiltro); // ¿Está el filtro en el campo No.?
        exit(not TempLCustomer.IsEmpty); // Si no está vacía es que cumple el Filtro, y devuelve 'true'
    end;


    //------------------------- OTRAS FUNCIONES DELICADAS -------------------------//
    local procedure OtrasFuncionesDelicadasF()
    var
        rlSalesHeader: Record "Sales Header";
        rlSalesInvoiceHeader: Record "Sales Invoice Header";
    begin
        rlSalesInvoiceHeader.TransferFields(rlSalesHeader);
    end;


    local procedure ListasyDiccionariosF()
    var
        xLista: List of [Code[20]];
        xlDic: Dictionary of [Code[20], Decimal];
        xlDic2: Dictionary of [Code[20], List of [Decimal]];
    begin

    end;
}
