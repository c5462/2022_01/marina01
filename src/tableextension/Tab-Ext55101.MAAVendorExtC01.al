/// <summary>
/// TableExtension "MAAVendorExtC01" (ID 50101) extends Record Vendor. Ejemplo del uso de variables globales.
/// </summary>
tableextension 55101 MAAVendorExtC01 extends Vendor
{
    fields //En este caso no introducimos campos, solo queremos utilizar triggers de extensión de tabla para poder utilizar variables globales 
    {
    }

    //Me trae el mismo nombre que pongo en cliente, el último que pongo
    trigger OnAfterInsert()
    var
        //Como la Codeunit es SingleInstance ya la tengo en memoria, de haber pasado por el cliente
        culMAAVariablesGlobalesC01: Codeunit MAAVariablesGlobalesC01;
    begin
        Rec.Validate(Name, culMAAVariablesGlobalesC01.NombreF());
    end;

}
