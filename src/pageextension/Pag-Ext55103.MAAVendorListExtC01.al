/// <summary>
/// PageExtension MAAVendorListExtC01 (ID 55103) extends Record Vendor List.
/// </summary>
pageextension 55103 "MAAVendorListExtC01" extends "Vendor List"
{
    layout
    {
    }

    actions
    {
        addlast(processing)
        {
            action(MAAImprimirFacturaC01)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin
                    rSalesInvoiceHeader.SetRange("No.", '103040');
                    rpGenericoVentas.SetTableView(rSalesInvoiceHeader);
                    rpGenericoVentas.Run();
                end;
            }
            action(MAAImprimirAlbaranC01)
            {
                ApplicationArea = All;

                trigger OnAction()
                begin
                    rSalesShipmentHeader.SetRange("No.", '102056');
                    rpGenericoVentas.SetTableView(rSalesShipmentHeader);
                    rpGenericoVentas.Run();
                end;
            }
        }
    }
    var
        rSalesShipmentHeader: Record "Sales Shipment Header";
        rSalesInvoiceHeader: Record "Sales Invoice Header";
        rpGenericoVentas: Report MAAGenericoVentasC01;
}
