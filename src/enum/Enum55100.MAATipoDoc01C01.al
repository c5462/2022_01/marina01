/// <summary>
/// Enum MAATipoDoc01C01 (ID 50100).
/// </summary>
enum 55100 "MAATipoDoc01C01"
{
    value(0; " ")
    {
    }
    value(1; Producto)
    {
    }
    value(2; Cuenta)
    {
    }
    value(3; Activo)
    {
    }
}
