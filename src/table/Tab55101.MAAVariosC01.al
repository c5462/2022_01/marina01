/// <summary>
/// Table "MAAVariosC01" (ID 50101). Tabla de Registro de Vacunas y Otros.
/// </summary>
table 55101 MAAVariosC01
{
    Caption = 'Tabla de Vacunas';
    DataClassification = SystemMetadata;
    //Relacionamos la tabla con la lista
    LookupPageId = MAAListaVariosC01; //Lo que se muestra al pulsar "Seleccionar de la lista completa"
    DrillDownPageId = MAAListaVariosC01;

    fields
    {
        field(1; Tipo; Enum MAATipoC01)
        {
            Caption = 'Tipo de Vacuna';
            DataClassification = SystemMetadata;
        }
        field(2; Codigo; Code[20])
        {
            Caption = 'Código';
            DataClassification = SystemMetadata;
        }
        field(3; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = SystemMetadata;
        }
        field(4; Bloqueado; Boolean) // Indica si un registro está bloqueado
        {
            Caption = 'Registro Bloqueado';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(5; PeriodoSegundaDosis; DateFormula) // Tiempo entre vacunas
        {
            Caption = 'Periodo segunda vacuna';
            DataClassification = OrganizationIdentifiableInformation;
        }
    }
    //La clave principal tiene dos segmentos porque es necesario en este caso.
    keys
    {
        key(PK; "Tipo", Codigo)
        {
            Clustered = true;
        }
    }
    //Los campos que aparecerán en el grupo desplegable, cuando lo enlacemos con la otra tabla. 
    fieldgroups
    {
        fieldgroup(DropDown; Codigo, Descripcion, PeriodoSegundaDosis)
        {

        }
    }

    trigger OnDelete()
    begin
        //Si el registro no está bloqueado, no permitas borrarlo (es como hacer un if)
        Rec.TestField(Bloqueado, true);
    end;
}
