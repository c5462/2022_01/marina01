/// <summary>
/// Page MAALogsC01 (ID 50107). Página para mostrar los logs de la tabla de eventos, rellenada en la Codeunit Lenta.
/// </summary>
page 55107 "MAAListaLogsC01"
{
    ApplicationArea = All;
    Caption = 'Lista Logs MAA';
    PageType = List;
    SourceTable = MAALogC01;
    UsageCategory = Lists;
    InsertAllowed = false;
    ModifyAllowed = false;
    DeleteAllowed = true; //Solo permite borrar logs
    SourceTableView = sorting(SystemCreatedAt) order(descending); //Que ordene por el SystemCreatedAt más reciente

    layout
    {
        area(content)
        {
            repeater(General)
            {
                field(SystemCreatedAt; Rec.SystemCreatedAt)
                {
                    ToolTip = 'Specifies the value of the SystemCreatedAt field.';
                    ApplicationArea = All;
                }
                field(Mensaje; Rec.Mensaje)
                {
                    ToolTip = 'Specifies the value of the Mensaje field.';
                    ApplicationArea = All;
                }
            }
        }
    }
}
