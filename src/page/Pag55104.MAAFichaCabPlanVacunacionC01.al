/// <summary>
/// Page "MAAFichaCabPlanVacunacionC01" (ID 50104). Página para crear el documento del Plan de Vacunación.
/// </summary>
page 55104 MAAFichaCabPlanVacunacionC01
{
    Caption = 'Ficha Cab. Plan de Vacunación MAA';
    PageType = Document;
    SourceTable = MAACabPlanVacunacionC01;
    UsageCategory = Documents;
    ApplicationArea = All;

    layout
    {
        area(content)
        {
            group(General)
            {
                field(CodigoCabecera; Rec.CodigoCabecera)
                {
                    ToolTip = 'Introduce el Código del plan de vacunación.';
                    ApplicationArea = All;
                }
                field(Descripcion; Rec.Descripcion)
                {
                    ToolTip = 'Descripción del plan de vacunación.';
                    ApplicationArea = All;
                }
                field(FechaInicioVacunacionPlan; Rec.FechaInicioVacunacionPlan)
                {
                    ToolTip = 'Indica la Fecha de Inicio Planificada para la vacunación.';
                    ApplicationArea = All;
                }
                field(EmpresaVacunadora; Rec.EmpresaVacunadora)
                {
                    ToolTip = 'Indica que empresa llevó a cabo la vacunación.';
                    ApplicationArea = All;
                }
                field(NombreEmpresaVacunadora; Rec.NombreEmpresaVacunadoraF()) //Cogemos el campo de una funcion, por eso, como no es un campo que exista en mi tabla hay que ponerle Caption
                {
                    Caption = 'Nombre de la empresa vacunadora';
                    ApplicationArea = All;
                    ToolTip = 'Aquí aparecerá el nombre del proveedor que realizará la vacunación.';
                }
            }
            //Subpágina, va dentro del área de contenido del documento
            part(Lineas; MAASubPageLinVacunacionC01)
            {
                ApplicationArea = All;
                SubPageLink = CodigoLineas = field(CodigoCabecera);
                //SubPageView = order(descending); //Con esta propiedad le digo como quiero que se muestre la subpágina
            }
        }
    }

    //---------------- ZONA DE ACTIONS (BOTONES) ----------------//
    actions
    {
        area(Processing)
        {
            action(EjemploVariableRef01)
            {
                Caption = 'Ejemplo variable referencia';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culMAAFuncionesAppC01: Codeunit MAAFuncionesAppC01;
                    xlTexto: Text;
                begin
                    xlTexto := 'Prueba Marina Alfaro'; //Cambiaré su valor dentro de la función
                    culMAAFuncionesAppC01.EjemploVariablesReferenciaF(xlTexto);
                    Message(xlTexto);
                end;
            }
            action(EjemploVariableObjeto01)
            {
                Caption = 'Ejemplo variable objeto';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culMAAFuncionesAppC01: Codeunit MAAFuncionesAppC01;
                    xlTexto: TextBuilder;
                begin
                    xlTexto.Append('Prueba Marina Alfaro'); //No cambio su valor, lo concateno dentro de la función
                    culMAAFuncionesAppC01.EjemploVarObjetosReferenciaF(xlTexto);
                    Message(xlTexto.ToText());
                end;
            }
            action(EjemploBingo)
            {
                Caption = 'Bingo';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culMAAFuncionesAppC01: Codeunit MAAFuncionesAppC01;
                begin
                    culMAAFuncionesAppC01.BingoF();
                end;
            }

            action(PruebaFuncion1Plano)
            {
                Caption = 'Prueba función en primer plano';
                ApplicationArea = All;

                trigger OnAction()
                var
                    culMAACodeunitLentaC01: Codeunit MAACodeunitLentaC01;
                begin
                    //Codeunit.Run(Codeunit::MAACodeunitLentaC01); //Otra forma de ejecutarla
                    culMAACodeunitLentaC01.Run();
                    Message('Proceso inicializado.');
                end;
            }

            action(PruebaFuncion2Plano)
            {
                Caption = 'Prueba función en segundo plano';
                ApplicationArea = All;

                trigger OnAction()
                var
                    xlSesionIniciada: Integer;
                begin
                    //Para ejecutar la Codeunit en una sesión en segundo plano
                    StartSession(xlSesionIniciada, Codeunit::MAACodeunitLentaC01);
                    Message('Proceso inicializado en segundo plano.');
                end;
            }

            // Prueba de CodeunitLenta a través de la tabla MAALogC01
            action(Logs)
            {
                ApplicationArea = All;
                Caption = 'Logs';
                Image = Log;
                //Ejecuta la página lista de Logs
                RunObject = page MAAListaLogsC01;
            }

            action(CrearLineasBulk) // Ejemplo para ver que Bulk tarda menos (inserta el dataset entero)
            {
                ApplicationArea = All;
                Caption = 'Crear Líneas Bulk';
                Image = Create;

                trigger OnAction()
                var
                    rlMAALinPlanVacunacionC01: Record MAALinPlanVacunacionC01;
                    xlInit: DateTime;
                    i: Integer;
                begin
                    xlInit := CurrentDateTime; //Fecha y hora actual

                    for i := 1 to 10000 do begin
                        rlMAALinPlanVacunacionC01.Init(); //Limpia y rellena si hay InitValues
                        rlMAALinPlanVacunacionC01.Validate(CodigoLineas, Rec.CodigoCabecera);
                        rlMAALinPlanVacunacionC01.Validate(NoLinea, i);
                        rlMAALinPlanVacunacionC01.Insert(true); //Para insertar la PK
                    end;
                    Message('Tiempo que ha tardado en ejecutar: %1', (CurrentDateTime - xlInit));
                end;
            }

            action(CrearLineasNoBulk) // Ejemplo para ver que línea a línea tarda más (hace llamadas a SQL)
            {
                ApplicationArea = All;
                Caption = 'Crear Líneas no Bulk';
                Image = Create;

                trigger OnAction()
                var
                    rlMAALinPlanVacunacionC01: Record MAALinPlanVacunacionC01;
                    xlInit: DateTime;
                    i: Integer;
                begin
                    xlInit := CurrentDateTime; //Fecha y hora actual

                    for i := 1 to 10000 do begin
                        rlMAALinPlanVacunacionC01.Init();
                        rlMAALinPlanVacunacionC01.Validate(CodigoLineas, Rec.CodigoCabecera);
                        rlMAALinPlanVacunacionC01.Validate(NoLinea, i);

                        // Este 'if' me hace que llame a SQL cada vez que pregunto (interrumpo el 'for')
                        if not rlMAALinPlanVacunacionC01.Insert(true) then begin
                            Error('No se ha podido insertar la línea %1 del plan %2', rlMAALinPlanVacunacionC01.NoLinea, rlMAALinPlanVacunacionC01.CodigoLineas);
                        end
                    end;
                    Message('Tiempo que ha tardado en ejecutar: %1', (CurrentDateTime - xlInit));
                end;
            }

            action(ModificarLineasBulk) // Ejemplo para ver que Bulk tarda menos 
            {
                ApplicationArea = All;
                Caption = 'Modificar Líneas Bulk';
                Image = Create;

                trigger OnAction()
                var
                    rlMAALinPlanVacunacionC01: Record MAALinPlanVacunacionC01;
                    xlInit: DateTime;
                begin
                    xlInit := CurrentDateTime; //Fecha y hora actual
                    rlMAALinPlanVacunacionC01.SetRange(CodigoLineas, Rec.CodigoCabecera);
                    rlMAALinPlanVacunacionC01.ModifyAll(FechaVacuna, Today, true);
                    Message('Tiempo que ha tardado en ejecutar: %1', (CurrentDateTime - xlInit));
                end;
            }

            action(ModificarLineasNoBulk) // Ejemplo para ver que línea a línea tarda más
            {
                ApplicationArea = All;
                Caption = 'Modificar Líneas no Bulk';
                Image = Create;

                trigger OnAction()
                var
                    rlMAALinPlanVacunacionC01: Record MAALinPlanVacunacionC01;
                    xlInit: DateTime;
                begin
                    xlInit := CurrentDateTime; //Fecha y hora actual
                    rlMAALinPlanVacunacionC01.SetRange(CodigoLineas, Rec.CodigoCabecera);

                    //FindSet(true, false) porque voy a modificar un campo del dataset que no es PK ni filtro
                    if rlMAALinPlanVacunacionC01.FindSet(true, false) then begin
                        repeat
                            rlMAALinPlanVacunacionC01.Validate(FechaVacuna, Today + 1);
                            rlMAALinPlanVacunacionC01.Modify(true);
                        until rlMAALinPlanVacunacionC01.Next() = 0;
                    end;
                    Message('Tiempo que ha tardado en ejecutar: %1', (CurrentDateTime - xlInit));
                end;
            }
        }
    }
}
