
/// <summary>
/// Codeunit MAAActualizarAppC01 (ID 50101). Se ejecutará cuando actualicemos la App.
/// </summary>
codeunit 55101 "MAAActualizarAppC01"
{
    Subtype = Upgrade;

    trigger OnCheckPreconditionsPerCompany()
    begin

    end;

    trigger OnCheckPreconditionsPerDatabase()
    begin

    end;

    // ------- Estos son los que más se utilizan, ya que dentro podrías hacer el chequeo de precondiciones -------//
    // ------- y las verificaciones posteriores a la actualización (en Tecon se hace con estas dos) --------------//

    trigger OnUpgradePerCompany()
    begin

    end;

    trigger OnUpgradePerDatabase()
    begin

    end;
    // -----------------------------------------------------------------------------------------------------------//

    trigger OnValidateUpgradePerCompany()
    begin

    end;

    trigger OnValidateUpgradePerDatabase()
    begin

    end;
}