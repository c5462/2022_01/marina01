/// <summary>
/// Table MAAConfiguracionC01 (ID 50100). Tabla de configuración.
/// </summary>
table 55100 "MAAConfiguracionC01"
{
    Caption = 'Configuración de la app 01 del curso';
    DataClassification = OrganizationIdentifiableInformation;

    fields
    {
        field(1; Id; Code[10])
        {
            Caption = 'Id. Registro';
            DataClassification = SystemMetadata;
        }
        field(2; CodClienteWeb; Code[20])
        {
            Caption = 'Código Cliente para Web';
            DataClassification = SystemMetadata;
            TableRelation = Customer."No."; //TableRelation a la tabla de clientes (hacemos un lookup a dicha tabla)
            ValidateTableRelation = true;
        }
        field(3; TextoReg; Text[250])
        {
            Caption = 'Texto Registro';
            DataClassification = SystemMetadata;
        }
        field(4; TipoDoc; Enum MAATipoDoc01C01)
        {
            Caption = 'Tipo de Documento';
            DataClassification = SystemMetadata;
        }
        field(5; NombreCliente; Text[100])
        {
            Caption = 'Nombre del Cliente';
            FieldClass = FlowField; //Campo calculado
            CalcFormula = lookup(Customer.Name where("No." = field(CodClienteWeb)));
            Editable = false;
        }
        field(6; TipoTabla; Enum MAATipoTablaC01)
        {
            Caption = 'Tipo de Tabla';
            DataClassification = SystemMetadata;

            //Trigger de campo, solo se ejecuta si lo introducido por el usuario es distinto de lo que había
            trigger OnValidate()
            begin
                if xRec.TipoTabla <> Rec.TipoTabla then begin
                    Rec.Validate(CodTabla, ''); //Le insertamos un Blank al campo CodTabla
                end;
            end;
        }
        field(7; CodTabla; Code[20]) //Los códigos de las tablas suelen ser de [20]
        {
            Caption = 'CodTabla';
            DataClassification = SystemMetadata;
            //TableRelation condicional
            //En lugar de utilizar 'const' podríamos utilizar 'filter' pero eso es mejor cuando hay más de un valor por el que filtrar
            TableRelation = if (TipoTabla = const(Cliente)) Customer else
            if (TipoTabla = const(Proveedor)) Vendor else
            if (TipoTabla = const(Recurso)) Resource else
            if (TipoTabla = const(Empleado)) Employee else
            if (TipoTabla = const(Contacto)) Contact;
        }
        field(8; ColorFondo; Enum MAAColoresC01)
        {
            Caption = 'Color de Fondo';
            DataClassification = SystemMetadata;
        }
        field(9; ColorLetra; Enum MAAColoresC01)
        {
            Caption = 'Color de Letra';
            DataClassification = SystemMetadata;
        }
        field(10; UnidadesDisponibles; Decimal)
        {
            Caption = 'Número de unidades disponibles';
            FieldClass = FlowField; //Campo calculado con filtros (para lo cual necesitamos crear campos tipo filtro)
            CalcFormula = sum("Item Ledger Entry".Quantity where("Item No." = field(FiltroProducto), "Posting Date" = field(FiltroFecha), "Entry Type" = field(FiltroTipoMovimiento)));
        }
        //CAMPOS TIPO FILTRO (para usarlos en mi campo calculado)
        field(11; FiltroProducto; Code[20]) //Se pone Code[20] porque es el tipo de la PK de la tabla de productos
        {
            TableRelation = Item."No."; //Relacionamos con la tabla de productos, con su PK
            Caption = 'Filtro Producto';
            FieldClass = FlowFilter;
        }
        field(12; FiltroFecha; Date)
        {
            //Los campos de tipo fecha no se relacionan nunca con ninguna tabla (BC ya hace su tratamiento de fechas internamente)
            Caption = 'Filtro Fecha';
            FieldClass = FlowFilter;
        }
        field(13; FiltroTipoMovimiento; Enum "Item Ledger Entry Type")
        {
            Caption = 'Filtro Tipo Movimiento';
            FieldClass = FlowFilter;
        }
        field(14; IdPassword; Guid)
        {
            // No le ponemos Caption porque no lo vamos a mostrar (solo lo necesitamos para tener un Guid)
            DataClassification = SystemMetadata;
        }
        field(15; MiBlob; Blob) // Campo Blob para usarlo como "contenedor" de información en Streams
        {
            // No le ponemos Caption porque Blob no se suele mostrar
            DataClassification = SystemMetadata;
        }
    }

    //--------------------------- Zona de Keys ---------------------------//
    keys
    {
        key(PK; Id)
        {
            Clustered = true;
        }
    }
    fieldgroups
    {
    }

    //--------------------------- Zona de funciones ---------------------------//
    /// <summary>
    /// Función que recupera el registro de la tabla actual, y si no existe lo crea. 
    /// OJO!! Solamente se hace así en las tablas de Configuración.
    /// </summary>
    /// <returns>Return value of type Boolean.</returns>
    procedure GetF(): Boolean
    begin
        if not Rec.Get() then begin
            Rec.Init();
            Rec.Insert(true);
        end;
        exit(true);
    end;

    /// <summary>
    /// Función que devuelve el nombre del cliente, pasado en el parámetro pCodCte. Si no existe devolverá un texto vacío.
    /// </summary>
    /// <param name="pCodCte"></param> 
    /// <returns></returns> Devuelve cliente
    procedure NombreClienteF(pCodCte: Code[20]): Text
    var
        rlCustomer: Record Customer; //Variable local de tipo Registro (rlXXX)
    begin
        if rlCustomer.Get(pCodCte) then begin
            exit(rlCustomer.Name);
        end;
    end;

    /// <summary>
    /// Función que obtiene el nombre de la tabla que elijamos en el campo Tipo de Tabla. 
    /// </summary>
    /// <param name="pTipoTabla">Enum MAATipoTablaC01.</param>
    /// <param name="pCodTabla">Code[20].</param>
    /// <returns>Return value of type Text.</returns>
    procedure NombreTablaSelecF(pTipoTabla: Enum MAATipoTablaC01; pCodTabla: Code[20]): Text
    var
        rlMAAConfiguracionC01: Record MAAConfiguracionC01;
        rlContact: Record Contact;
        rlEmployee: Record Employee;
        rlVendor: Record Vendor;
        rlResource: Record Resource;
    begin
        case pTipoTabla of
            (pTipoTabla::Cliente):
                begin
                    exit(rlMAAConfiguracionC01.NombreClienteF(pCodTabla));
                end;
            (pTipoTabla::Contacto):
                begin
                    if rlContact.get(pCodTabla) then begin
                        exit(rlContact.Name)
                    end;
                end;
            (pTipoTabla::Empleado):
                begin
                    if rlEmployee.get(pCodTabla) then begin
                        exit(rlEmployee.FullName());
                    end;
                end;
            (pTipoTabla::Proveedor):
                begin
                    if rlVendor.get(pCodTabla) then begin
                        exit(rlVendor.Name);
                    end;
                end;
            (pTipoTabla::Recurso):
                begin
                    if rlResource.get(pCodTabla) then begin
                        exit(rlResource.Name);
                    end;
                end;
        end;
    end;


    /// <summary>
    /// Introducimos (set) un password que se guardará utilizando como clave un Guid.
    /// </summary>
    /// <param name="pPassword">Text.</param>
    [NonDebuggable]
    procedure PassWordF(pPassword: Text)
    begin
        //Cada vez que asigno el password me da un nuevo Guid
        Rec.Validate(IdPassword, CreateGuid());
        Rec.Modify(true);
        //Lo guardamos en el Isolated Storage de SQL, a nivel de empresa 
        //(si lo guardamos a nivel usuario solo lo podría recuperar ese usuario en concreto)
        IsolatedStorage.Set(Rec.IdPassword, pPassword, DataScope::Company);
    end;

    /// <summary>
    /// Obtenemos (get) un password que se guardó utilizando como clave un Guid.
    /// </summary>
    /// <returns>Return variable xSalida of type Text.</returns>
    [NonDebuggable]
    procedure PassWordF() xSalida: Text
    begin
        if not IsolatedStorage.Get(Rec.IdPassword, DataScope::Company, xSalida) then begin
            Error('No se encuentra el Password en la BBDD');
        end;
    end;
}