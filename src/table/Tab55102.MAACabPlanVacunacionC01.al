/// <summary>
/// Table "MAACabPlanVacunacionC01" (ID 50102). Cabecera del documento del Plan de Vacunación.
/// </summary>
table 55102 MAACabPlanVacunacionC01
{
    Caption = 'Cab. Plan de Vacunación';
    DataClassification = SystemMetadata;
    LookupPageId = MAAListaCabPlanVacunacionC01; // Página que se abrirá por defecto

    fields
    {
        field(1; CodigoCabecera; Code[20])
        {
            Caption = 'Código';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(2; Descripcion; Text[50])
        {
            Caption = 'Descripción';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(3; FechaInicioVacunacionPlan; Date)
        {
            Caption = 'Fecha Inicio Planificada Vacunación';
            DataClassification = OrganizationIdentifiableInformation;
        }
        field(4; EmpresaVacunadora; Code[20])
        {
            Caption = 'Empresa Vacunadora';
            DataClassification = OrganizationIdentifiableInformation;
            TableRelation = Vendor."No.";
        }
    }
    keys
    {
        key(PK; CodigoCabecera)
        {
            Clustered = true;
        }
    }

    //Borra todas las lineas asociadas a una cabecera concreta
    trigger OnDelete()
    var
        rlMAALinPlanVacunacionC01: Record MAALinPlanVacunacionC01;
    begin
        rlMAALinPlanVacunacionC01.SetRange(CodigoLineas, Rec.CodigoCabecera);
        if not rlMAALinPlanVacunacionC01.IsEmpty then begin //para saber si hay líneas en un dataset
            rlMAALinPlanVacunacionC01.DeleteAll(true);  // Instrucción Bulk
        end;
    end;


    //----------------------- PROCEDURES -----------------------//
    /// <summary>
    /// NombreEmpresaVacunadoraF. Función que devuelve el nombre de la empresa vacunadora. 
    /// </summary>
    /// <returns>Return value of type Text[50].</returns>
    procedure NombreEmpresaVacunadoraF(): Text
    var
        rlVendor: Record Vendor;
    begin
        if rlVendor.Get(Rec.EmpresaVacunadora) then begin
            exit(rlVendor.Name);
        end;
    end;
}
