/// <summary>
/// Enum MAATipoTablaC01 (ID 50101).
/// </summary>
enum 55101 "MAATipoTablaC01"
{
    value(0; Cliente)
    {
        Caption = 'Cliente';
    }
    value(10; Proveedor)
    {
        Caption = ' Proveedor';
    }
    value(20; Recurso)
    {
        Caption = ' Recurso';
    }
    value(30; Empleado)
    {
        Caption = ' Empleado';
    }
    value(40; Contacto)
    {
        Caption = ' Contacto';
    }
}
