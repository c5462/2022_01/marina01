/// <summary>
/// Report "MAAListadoProyectosC01" (ID 50101). Informe de proyectos.
/// </summary>
report 55101 MAAListadoProyectosC01
{
    UsageCategory = Administration;
    ApplicationArea = All;
    DefaultLayout = RDLC; //Indicas de que tipo quieres que sea el informe
    RDLCLayout = './layout/ListadoProyectos.rdlc';
    AdditionalSearchTerms = 'Listado Proyectos';
    Caption = 'Listado Proyectos';

    dataset
    {
        dataitem(Proyectos; Job) //Nuestro item Proyectos dentro del área del informe
        {
            //DataItemTableView = sorting("No."); // Ordenar por campo No.
            RequestFilterFields = "No.", "Creation Date", Status; //Filtros de página
            column(No_Proyectos; "No.")
            {
            }
            column(BilltoCustomerNo_Proyectos; "Bill-to Customer No.")
            {
            }
            column(BilltoName_Proyectos; "Bill-to Name")
            {
            }
            column(CreationDate_Proyectos; Format("Creation Date"))
            {
            }
            column(Description_Proyectos; Description)
            {
            }
            column(Status_Proyectos; Status)
            {
            }
            column(Color_Proyectos; xColor)
            {
            }

            dataitem(TareasProyecto; "Job Task") //Item de tareas dentro de proyectos
            {
                RequestFilterFields = "Job Task No.", "Job Task Type";
                DataItemLink = "Job No." = field("No.");
                column(JobTaskNo_Tareas; "Job Task No.")
                {
                }
                //Deberíamos poner el Caption en cada columna, de la siguiente forma: 
                // column(JobTaskNo_Tareas_Caption; FieldCaption("Job Task No."))
                // {

                // }
                column(JobTaskType_Tareas; "Job Task Type")
                {
                }
                column(Description_Tareas; Description)
                {
                }
                column(StartDate_Tareas; Format("Start Date"))
                {
                }
                column(EndDate_Tareas; Format("End Date"))
                {
                }

                dataitem(LineasPlanProyecto; "Job Planning Line") //Item de lineas de planificacion dentro de proyectos
                {
                    RequestFilterFields = "No.", "Planning Date", Type;
                    DataItemLink = "Job No." = field("Job No."), "Job Task No." = field("Job Task No.");
                    column(LineNo_LineasPlanProyecto; "Line No.")
                    {
                    }
                    column(No_LineasPlanProyecto; "No.")
                    {
                    }
                    column(Description_LineasPlanProyecto; Description)
                    {
                    }
                    column(Quantity_LineasPlanProyecto; Quantity)
                    {
                    }
                    column(PlanningDate_LineasPlanProyecto; Format("Planning Date"))
                    {
                    }
                    column(LineAmount_LineasPlanProyecto; "Line Amount")
                    {
                    }
                    column(Type_LineasPlanProyecto; "Type")
                    {
                    }

                    dataitem(MovProyecto; "Job Ledger Entry") //Item de movs. de proyecto dentro de proyectos
                    {
                        DataItemLink = "Entry No." = field("Job Ledger Entry No.");
                        column(EntryNo_MovProyecto; "Entry No.")
                        {
                        }
                        column(Type_MovProyecto; "Type")
                        {
                        }
                        column(EntryType_MovProyecto; "Entry Type")
                        {
                        }
                        column(LedgerEntryType_MovProyecto; "Ledger Entry Type")
                        {
                        }
                        column(LineType_MovProyecto; "Line Type")
                        {
                        }
                        column(DocumentNo_MovProyecto; "Document No.")
                        {
                        }
                        column(DocumentDate_MovProyecto; Format("Document Date"))
                        {
                        }
                    }
                }
            }
            //Creamos un trigger de Proyectos (afectara a las columnas de proyectos en report builder)
            trigger OnAfterGetRecord()
            begin
                case Proyectos.Status of
                    Proyectos.Status::Completed:
                        begin
                            xColor := 'Green'
                        end;
                    Proyectos.Status::Open:
                        begin
                            xColor := 'Blue'
                        end;
                    Proyectos.Status::Planning:
                        begin
                            xColor := 'Yellow'
                        end;
                    Proyectos.Status::Quote:
                        begin
                            xColor := 'Red'
                        end;
                end;
            end;
        }
    }

    requestpage
    {
        layout
        {
            area(Content)
            {
                group(GroupName)
                {

                }
            }
        }
    }

    //Zona de variables globales 
    var
        xColor: Text;
}